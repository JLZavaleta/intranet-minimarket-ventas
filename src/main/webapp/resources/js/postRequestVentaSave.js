$( document ).ready(function() {
	
	// SUBMIT FORM
    $("#customerForm").submit(function(event) {
		// Prevent the form from submitting via the browser.
		event.preventDefault();
		ajaxPost();
	});
    
    
    function ajaxPost(){
    	var detalle = null;
    	var detalleVenta = null;    	
    	var tabla = document.getElementById("mydataDetalleVenta");
    	var tbody = document.getElementById("listaDetalleVenta");
    	for(var i=0;i<tbody.rows.length;i++){
    		var miFila = tbody.getElementsByTagName("tr")[i];
    		var miCelda = miFila.getElementsByTagName("td")[0];
    		//---------------------------------------------------------
    		var miCeldaCantidad = miFila.getElementsByTagName("td")[4];
    		var inptcantidad = miCeldaCantidad.getElementsByTagName("input")[0];
    		//---------------------------------------------------------
    		var inpt = miCelda.getElementsByTagName("input")[0];
    		var miDato = miCeldaCantidad.firstChild.nodeValue;
    	
    		detalleVenta = Array(inpt.value,inptcantidad.value);
    	}
    	// PREPARE FORM DATA
    	detalle = [
    		//for(var i=0;i<detalleVenta.lenght;i++){
    		{
    		producto:{
    			codproducto : detalleVenta[0]
    			},
    		cantidad : detalleVenta[1]
    		},
    	//}
    		];
    	//console.log(JSON.parse(JSON.stringify(detalle)));
    	//console.log(JSON.parse(detalleVenta));
    	//console.log(JSON.stringify(detalleVenta));
    	var formData = {
    			cliente : {codcliente : $("#txtidcliente").val()},
    			trabajador : {codtrabajador : $("#txtidTrabajador").val()},
    			tipodoc : $("#cboTipoDocumento").val(),
    			f_emision : $("#txtFechaVenta").val(),
    			detalleVenta : detalle
    	};
    	/*
    	// DO POST
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "http://localhost:8083/compras/Venta/nuevaVenta",//window.location + "api/cliente/grabar",
			data : JSON.stringify(formData),
			dataType : 'json',
			success : function(result) {
				if(result.status == "Done"){
					$("#postResultDiv").html("<p style='background-color:#7FA7B0; color:white; padding:20px 20px 20px 20px'>" + 
												"Post Successfully! <br>" + "</p>");
				}else{
					$("#postResultDiv").html("<strong>Error</strong>");
				}
				console.log(result);
			},
			error : function(e) {
				alert("Error!")
				console.log("ERROR: ", e);
			}
		});
    	*/
    	// Reset FormData after Posting
    	resetData();

    }
    
    function resetData(){
		$("#txtNombre").val("");
		$("#txtApellidos").val("");
		$("#txtDireccion").val("");
		$("#txtCuidad").val("");
		$("#txtRegion").val("");
		$("#txtTelefono").val("");
		$("#txtUsuario").val("");
		$("#txtPassword").val("");
		$("#txtFechaTope").val("");
    }
})