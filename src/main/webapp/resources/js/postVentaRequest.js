$( document ).ready(function() {
	
	var URLBase = "http://localhost:8083/minimarket";
	
	// SUBMIT FORM
    $("#customerForm").submit(function(event) {
		event.preventDefault();
		validacion();
	});
    
    
    
    function ajaxPost(){
    	var detalle = null;
    	var detalleVenta = Array(); 

    	var tabla = document.getElementById("mydataDetalleVenta");
    	var tbody = document.getElementById("listaDetalleVenta");
    	for(var i=0;i<tbody.rows.length;i++){
    		var miFila = tbody.getElementsByTagName("tr")[i];
    		var miCelda = miFila.getElementsByTagName("td")[0];
    		//---------------------------------------------------------
    		var miCeldaCantidad = miFila.getElementsByTagName("td")[4];
    		var inptcantidad = miCeldaCantidad.getElementsByTagName("input")[0];
    		//---------------------------------------------------------
    		var inpt = miCelda.getElementsByTagName("input")[0];
    		var miDato = miCeldaCantidad.firstChild.nodeValue;  
        	detalle ={	
        			producto:{codproducto : inpt.value},
        			cantidad : inptcantidad.value
        			};
        	detalleVenta.push(detalle);
    	}
    	// PREPARE FORM DATA
    	
    	//console.log(JSON.parse(JSON.stringify(detalle)));
    	//console.log(JSON.parse(detalleVenta));
    	//console.log(JSON.stringify(detalleVenta));
    	var formData = {
    			cliente : {codcliente : $("#txtidcliente").val()},
    			//trabajador : {codtrabajador : $("#txtidTrabajador").val()},
    			tipodoc : $("#cboTipoDocumento").val(),
    			f_emision : $("#txtFechaVenta").val(),
    			detalleVenta : detalleVenta
    	};
    	console.log(JSON.stringify(formData));
    	
    	// DO POST
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			url :URLBase+"/Venta/nuevaVenta",//window.location + "api/cliente/grabar",
			data : JSON.stringify(formData),
			dataType : 'json',
			success : function(result) {
				if(result.status == "Done"){
					$("#postResultDiv").html("<div class='alert alert-success' role='alert'>Venta Realizada</div>");
				}else if(result.status == "Validacion"){
					$("#postResultDiv").html("<strong>"+result.data+"</strong>");
				}else{
					$("#postResultDiv").html("<strong>Error</strong>");
				}
				console.log(result);
			},
			error : function(e) {
				alert("Error!")
				console.log("ERROR: ", e);
			}
		});   	
    }
    
    function validacion(){
    	var txtnombreCliente = document.getElementById("txtnombreCliente");
    	if(txtnombreCliente.value == null || txtnombreCliente.value.length == 0 || /^\s+$/.test(txtnombreCliente.value)) {
    		$("#postResultDiv").html("<div class='alert alert-danger' role='alert' style='opacity:1;'>Debe ingresar un Cliente.</div>");
    		var myVar = setTimeout(borrar, 3000);
    		  return false;
    	}
    	var indice = document.getElementById("cboTipoDocumento").selectedIndex;
    	if(indice == null || indice == 0 ) {
    		$("#postResultDiv").html("<div class='alert alert-danger' role='alert'>Debe un Tipo de Documento.</div>");
    		var myVar = setTimeout(borrar, 3000);
    	  return false;
    	}
    	var txtFechaVenta = document.getElementById("txtFechaVenta");
    	if(txtFechaVenta.value == null || txtFechaVenta.value.length == 0 || /^\s+$/.test(txtFechaVenta.value)) {
    		$("#postResultDiv").html("<div class='alert alert-danger' role='alert'>Debe una Fecha de Emision.</div>");
    		var myVar = setTimeout(borrar, 3000);
    		  return false;
    	}
    	var listaDetalleVenta = document.getElementById("listaDetalleVenta");
    	if(listaDetalleVenta == null){
    		$("#postResultDiv").html("<div class='alert alert-danger' role='alert'>No ingreso ningun Producto.</div>");
    		var myVar = setTimeout(borrar, 3000);
    		return false;
    	}
    	ajaxPost();	

    }
    function borrar(){
    	$("#postResultDiv").animate({
    	    opacity: 0.25
    	  }, 2000, function() {
    		  $("#postResultDiv").html("")
    	  });
    }
})
