$( document ).ready(function() {
	
	var ListaDetalle = Array();
	var subTotal = Array();
	var calcular = Array();
	var tablaDetalleVenta = document.getElementById("listaDetalleVenta");
	var contSub = 0;
	var verSubtotal=0;
	var Total = 0; 
	var URLBase = "http://localhost:8083/minimarket";
	/*
	$("#btnAgredarCliente").click(function(event){
		event.preventDefault();
		var listCheckbox = document.getElementsByName("checkboxSeleccionadoCliente");
		for(var i =0;i<listCheckbox.length;i++){
			if(listCheckbox[i].checked==true){
				alert("TRUE");
			}else{
				alert("FALSE");
			}
		}
	});*/
	/*
	$("#btnAgredarProducto").click(function(event){
		event.preventDefault();
		var listCheckbox = document.getElementsByName("checkboxSeleccionadoProducto");
		for(var i =0;i<listCheckbox.length;i++){
			if(listCheckbox[i].checked==true){
				alert("TRUE");
			}else{
				alert("FALSE");
			}
		}
	});*/
	
	// DO GET
	function leer(){
		ajaxGet();
		ajaxGetProducto();
		
	}
	function ajaxGet(){
		var tabla = document.getElementById("lista");
		$.ajax({
			type : "GET",
			url :URLBase+"/Cliente/listar", ///window.location + "Cliente/listar",
			success: function(result){				
				if(result.status == "Done"){										
					$.each(result.data, function(i, customer){

						var fila = document.createElement("tr");
							
	                        var celcheckbox = document.createElement("td");
	                        var inputcheckbox = document.createElement("input");
	                        inputcheckbox.setAttribute("type","radio");
	                        inputcheckbox.setAttribute("id","checkbox"+i);
	                        inputcheckbox.setAttribute("name","checkboxSeleccionadoCliente");
	                        celcheckbox.appendChild(inputcheckbox); 
	                        fila.appendChild(celcheckbox);
	                       
	                        var celnombre = document.createElement("td");
	                        var txtnombre = document.createTextNode(customer.nombre);
	                        celnombre.appendChild(txtnombre);     
	                        fila.appendChild(celnombre);

	                        var celape_pater = document.createElement("td");
	                        var txtape_pater = document.createTextNode(customer.ape_pater);
	                        celape_pater.appendChild(txtape_pater);     
	                        fila.appendChild(celape_pater);

	                        var celape_mater = document.createElement("td");
	                        var txtape_mater = document.createTextNode(customer.ape_mater);
	                        celape_mater.appendChild(txtape_mater);     
	                        fila.appendChild(celape_mater);

	                        var celdireccion = document.createElement("td");
	                        var txtdireccion = document.createTextNode(customer.direccion);
	                        celdireccion.appendChild(txtdireccion);     
	                        fila.appendChild(celdireccion);

	                        var celtelefono = document.createElement("td");
	                        var txttelefono = document.createTextNode(customer.telefono);
	                        celtelefono.appendChild(txttelefono);     
	                        fila.appendChild(celtelefono);

	                        tabla.appendChild(fila);
	                        
	                    	$("#btnAgredarCliente").click(function(event){
	                    		event.preventDefault();
	                    		if(inputcheckbox.checked==true){
	                    			document.getElementById("txtidcliente").value=customer.codcliente;
		                    		document.getElementById("txtnombreCliente").value=customer.nombre+" "+customer.ape_pater+" "+customer.ape_mater;
		                    		$("#mensaje").html("<div class='alert alert-success' role='alert'>Se agrego al Cliente.</div>");	
	                    		}	                    		
	                    	});
	                        
			        });
					console.log("Success: ", result);
					$("#mydataCliente").dataTable();
				}else{
					$("#getResultDiv").html("<strong>Error</strong>");
					console.log("Fail: ", result);
				}
				
			},
			error : function(e) {
				$("#getResultDiv").html("<strong>Error</strong>");
				console.log("ERROR: ", e);
			}
		});	
	}
	
	
	
	// DO GET
	function ajaxGetProducto(){
		var tabla = document.getElementById("listaPro");		
		$.ajax({
			type : "GET",
			url :URLBase+"/Producto/listarest", ///window.location + "Cliente/listar",
			success: function(result){
				if(result.status == "Done"){										
					$.each(result.data, function(i, customer){

						var fila = document.createElement("tr");
	                        
	                        var celcheckbox = document.createElement("td");
	                        var inputcheckbox = document.createElement("input");
	                        inputcheckbox.setAttribute("type","checkbox");
	                        inputcheckbox.setAttribute("id","checkbox"+i);
	                        inputcheckbox.setAttribute("name","checkboxSeleccionadoProducto");
	                        celcheckbox.appendChild(inputcheckbox); 
	                        fila.appendChild(celcheckbox);
	                       
	                        var celnombre = document.createElement("td");
	                        var txtnombre = document.createTextNode(customer.nombre);
	                        celnombre.appendChild(txtnombre);     
	                        fila.appendChild(celnombre);
	                        
	                        var celdecripcion = document.createElement("td");
	                        var txtdecripcion = document.createTextNode(customer.descripcion);
	                        celdecripcion.appendChild(txtdecripcion);     
	                        fila.appendChild(celdecripcion);

	                        var celcategoria = document.createElement("td");
	                        var txtcategoria = document.createTextNode(customer.categoria.descripcion);
	                        celcategoria.appendChild(txtcategoria);     
	                        fila.appendChild(celcategoria);
	                       	                        
	                        var celprecio = document.createElement("td");
	                        var txtprecio = document.createTextNode(customer.preciov);
	                        celprecio.appendChild(txtprecio);     
	                        fila.appendChild(celprecio);

	                        var celstock = document.createElement("td");
	                        var txtstock = document.createTextNode(customer.stock);
	                        celstock.appendChild(txtstock);     
	                        fila.appendChild(celstock);

	                        tabla.appendChild(fila);
	                        //"dblclick"
	                        $("#btnAgredarProducto").click(function(event){	                        	
	                        	var filaDetalle = document.createElement("tr");
	                 
	                        	var celnombreD = document.createElement("td");
		                        var txtnombreD = document.createTextNode(customer.nombre);
		                        var txtcodproductoD = document.createElement("input");
		                        	txtcodproductoD.setAttribute("type","hidden");
		                        	txtcodproductoD.setAttribute("id","cod");
		                        	txtcodproductoD.setAttribute("value",customer.codproducto);
		                        	celnombreD.appendChild(txtcodproductoD); 
		                        	celnombreD.appendChild(txtnombreD);   
		                        
		                        var celdecripcionD = document.createElement("td");
		                        var txtdecripcionD = document.createTextNode(customer.descripcion);
		                        celdecripcionD.appendChild(txtdecripcionD);     

		                        var celcategoriaD = document.createElement("td");
		                        var txtcategoriaD = document.createTextNode(customer.categoria.descripcion);
		                        celcategoriaD.appendChild(txtcategoriaD);     

		                        var celprecioD = document.createElement("td");
		                        var txtprecioD = document.createTextNode(customer.preciov);
		                        celprecioD.appendChild(txtprecioD);     

		                        var celCantidadD = document.createElement("td");
		                        var txtCantidadD = document.createElement("input");
		                        txtCantidadD.setAttribute("type","text");
		                        txtCantidadD.setAttribute("class","form-control");
		                        txtCantidadD.setAttribute("value","1");
		                        celCantidadD.appendChild(txtCantidadD);     

		                        var subTotal =customer.preciov*txtCantidadD.value;                        
	                        
		                        var celSubTotalD = document.createElement("td");
		                        var txtSubTotalD = document.createElement("input");
		                        txtSubTotalD.setAttribute("type","text");
		                        txtSubTotalD.setAttribute("class","form-control");
		                        txtSubTotalD.setAttribute("value",subTotal);
		                        txtSubTotalD.setAttribute("disabled","disabled");
		                        celSubTotalD.appendChild(txtSubTotalD);     
		                        		                        
		                        var celEliminarD = document.createElement("td");
		                        var txtEliminarD = document.createElement("input");
		                        txtEliminarD.setAttribute("type","button");
		                        txtEliminarD.setAttribute("value","Eliminar");
		                        txtEliminarD.setAttribute("class","btn btn-danger");
		                        celEliminarD.appendChild(txtEliminarD);     
		                        
		                        filaDetalle.appendChild(celnombreD);
		                        filaDetalle.appendChild(celdecripcionD);
		                        filaDetalle.appendChild(celcategoriaD);
		                        filaDetalle.appendChild(celprecioD);
		                        filaDetalle.appendChild(celCantidadD);
		                        filaDetalle.appendChild(celSubTotalD);
		                        filaDetalle.appendChild(celEliminarD);
		                        		                        		                        
	                        	if(inputcheckbox.checked==true){
	                        		contSub = contSub + subTotal;
	                        		tablaDetalleVenta.appendChild(filaDetalle);	
	                        		calcular[i] = subTotal;
	                        		inputcheckbox.checked=false;	                        		
	                			}	
	                        	
		                        Total = contSub+(contSub*0.18);
		                        txtCantidadD.addEventListener("blur", function(){	
		                        	var subTotal =customer.preciov*txtCantidadD.value;
		                        	
		                        	txtSubTotalD.setAttribute("value",subTotal);
		                        	
		                        	contSub = contSub-calcular[i];
		                        	
		                        	calcular[i]=subTotal;
		                        	
		                        	contSub = contSub + calcular[i];
		                        	
		                        	Total = contSub+(contSub*0.18);
		                        	
		                        	document.getElementById("txtSubTotal").value=contSub;
		                        	document.getElementById("txtTotal").value=Total;
		                        });
	                        	
	                        	txtEliminarD.addEventListener("click", function(){	                      		
	                        		
	                        		contSub = contSub-calcular[i];	
	                        		
	                        		Total = contSub+(contSub*0.18);
	                        		
		                        	tablaDetalleVenta.removeChild(filaDetalle);
		                        	
		                        	document.getElementById("txtSubTotal").value=contSub;
		                        	document.getElementById("txtTotal").value=Total;
	                        	});                     	
	                        		                        	
	                        	document.getElementById("txtSubTotal").value=contSub;
	                        	document.getElementById("txtTotal").value=Total;
	                		});
	                        
			        });
					console.log("Success: ", result);
					$("#mydataProducto").dataTable();
				}else{
					$("#getResultDiv").html("<strong>Error</strong>");
					console.log("Fail: ", result);
				}
			},
			error : function(e) {
				$("#getResultDiv").html("<strong>Error</strong>");
				console.log("ERROR: ", e);
			}
		});	
	}
	window.onload = leer;
});