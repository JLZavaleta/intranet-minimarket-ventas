$( document ).ready(function() {
	
	var URLBase = "http://localhost:8083/IntranetMiniMarket";
	
	// SUBMIT FORM
    $("#FormListProductosMasVendidos").submit(function(event) {
		event.preventDefault();
		ajaxPostListaProductos();
	});
        
    
    function ajaxPostListaProductos(){
    	var formData = {
    			limite : $("#txtlimite").val(),
    			fechaDesde : $("#txtfechadesde").val(),
    			fechaHasta : $("#txtfechahasta").val()
    	};

    	// DO POST
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			url :URLBase+"/Reportes/pdf/productosVenta",
			data : JSON.stringify(formData),
			dataType : 'json',
			success : function(result) {
				if(result.status == "Done"){
					
					var tabla = document.getElementById("listaProductosMasVendidos");
					
					$.each(result.data, function(i, customer){
					
						var fila = document.createElement("tr");					
	                   
						var celnombre = document.createElement("td");
	                    var txtnombre = document.createTextNode(customer.nombre);
	                    celnombre.appendChild(txtnombre);     
	                    fila.appendChild(celnombre);
	                    
	                    var celdecripcion = document.createElement("td");
	                    var txtdecripcion = document.createTextNode(customer.descripcion);
	                    celdecripcion.appendChild(txtdecripcion);     
	                    fila.appendChild(celdecripcion);
	
	                    var celcategoria = document.createElement("td");
	                    var txtcategoria = document.createTextNode(customer.categoria.descripcion);
	                    celcategoria.appendChild(txtcategoria);     
	                    fila.appendChild(celcategoria);
	                   	                        
	                    var celprecio = document.createElement("td");
	                    var txtprecio = document.createTextNode(customer.preciov);
	                    celprecio.appendChild(txtprecio);     
	                    fila.appendChild(celprecio);
	
	                    var celstock = document.createElement("td");
	                    var txtstock = document.createTextNode(customer.stock);
	                    celstock.appendChild(txtstock);     
	                    fila.appendChild(celstock);
	
	                    tabla.appendChild(fila);
					
					});
					$("#postResultDiv").html("<div class='alert alert-success' role='alert'>Listado Exitoso</div>");
				}else if(result.status == "Fail"){
					$("#postResultDiv").html("<strong>"+result.data+"</strong>");
				}else{
					$("#postResultDiv").html("<strong>Error</strong>");
				}
				console.log(result);
			},
			error : function(e) {
				alert("Error!")
				console.log("ERROR: ", e);
			}
		});   	
    }
    
    function borrar(){
    	$("#postResultDiv").animate({
    	    opacity: 0.25
    	  }, 2000, function() {
    		  $("#postResultDiv").html("")
    	  });
    }
    //window.onload = leer;
})