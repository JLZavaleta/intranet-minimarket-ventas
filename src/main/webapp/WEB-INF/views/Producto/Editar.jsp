<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ page session="false" %>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetHeader.jsp"></jsp:include>  
	<section class="content-header">
		<h1>
			Editar Producto <small>Editar Datos del Producto</small>
		</h1> 
	</section>
	<section class="content">
	
	   <div class="box box-info">
         <div class="box-header with-border">  
       		<a class="box-title" href="${pageContext.request.contextPath}/Producto/lista">Lista Productos</a>
         </div>
	<frm:form cssClass="form-horizontal" method="POST" action="GrabarEditar">
			<div class="box-body">
                <div class="form-group">
                  <div class="col-sm-10">              
                    <frm:hidden path="codproducto"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Nombre Producto</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" placeholder="Nombre Producto" path="nombre"></frm:input>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Descripcion</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" placeholder="Descripcion" path="descripcion"></frm:input>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Categoria</label>

                  <div class="col-sm-10">
           
	                  <frm:select cssClass="form-control" path="categoria.idtipo">
	                  <c:forEach items="${listaCategorias}" var="cat">
                    		<frm:option value="${cat.idtipo}">${cat.descripcion}</frm:option>
                      </c:forEach>		
                      </frm:select>
                    
                  </div>
                </div>
                 <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Precio</label>

                  <div class="col-sm-10">
                  <div class="input-group">
                  	<span class="input-group-addon">S/.</span>
                  		<frm:input cssClass="form-control" placeholder="Precio" path="preciov"></frm:input>
                  		<span class="input-group-addon">.00</span>
                  </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Unidades en Existencia</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" type="number" step="1" min="1" max="100000" placeholder="Unidades en Existencia" path="stock"></frm:input>
                  </div>
                </div>	
			</div>
			<!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" class="btn btn-info pull-right" value="Guardar"/>
              </div>
            <!-- /.box-footer -->
            <h4>${mensaje}</h4>
	</frm:form>
	</div>
	</section>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetFooter.jsp"></jsp:include>  