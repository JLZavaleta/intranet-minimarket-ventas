<jsp:include page="/WEB-INF/views/Includes/MasterIntranetHeader.jsp"></jsp:include>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ page session="false" %>

	<section class="content-header">
		<h1>
			Nuevo Cliente <small>Ingresar Datos del Cliente</small>
		</h1> 
	</section>
	<section class="content"> 
				
          <div class="box box-info">
            <div class="box-header with-border">  
              <a class="box-title" href="${pageContext.request.contextPath}/Cliente/lista">Lista Clientes</a>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <frm:form cssClass="form-horizontal" method="POST" action="GrabarNuevo">
              <div class="box-body">
              	<div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Dni</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" placeholder="Documento de Identidad" path="dni"></frm:input>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Nombre Cliente</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" placeholder="Nombre Cliente" path="nombre"></frm:input>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Apellido Paterno</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" placeholder="Apellido Paterno" path="ape_pater"></frm:input>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Apellido materno</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" placeholder="Apellido materno" path="ape_mater"></frm:input>
                  </div>
                </div>
                <!-- codcliente, nombre, ape_pater, ape_mater, direccion, telefono -->
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Direccion</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" placeholder="Direccion" path="direccion"></frm:input>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Telefono</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" placeholder="Telefono" path="telefono"></frm:input>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" class="btn btn-info pull-right" value="Guardar"/>
              </div>
              <!-- /.box-footer -->
              <h4>	${mensaje} </h4>
            </frm:form>
          </div>
          </section>
<!--

//-->
</script>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetFooter.jsp"></jsp:include>  