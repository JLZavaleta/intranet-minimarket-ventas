<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ page session="false" %>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetHeader.jsp"></jsp:include>  
	<section class="content-header">
		<h1>
			Lista de Clientes <small><a class="box-subtitle" href="${pageContext.request.contextPath}/Menu">Menu</a></small>
		</h1> 
	</section>
 
   <section class="content">
	    
   <div class="box box-info">
       <div class="box-header with-border">
     	  <a class="box-title" href="${pageContext.request.contextPath}/Cliente/Nuevo">Nuevo Cliente</a>
   	   </div>
    
       <div class="box-header with-border">
			<%
			if(request.getParameter("msj")!=null)
			out.print("<p>"+request.getParameter("msj")!=null?request.getParameter("msj"):""+"</p>");
			%>
   	   </div>  
  <div class="box-body">	
	<div class="table-responsive">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>DNI</th>			
				<th>nombre</th>
				<th>ape_pater</th>
				<th>ape_mater</th>
				<th>direccion</th>
				<th>telefono</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listaClientes}" var="cli">
				<tr style="">
					<td>${cli.dni}</td>					
					<td>${cli.nombre}</td>
					<td>${cli.ape_pater}</td>
					<td>${cli.ape_mater}</td>
					<td>${cli.direccion}</td>
					<td>${cli.telefono}</td>	
					<td style="width:100px; height:45px;">
					<a class="btn btn-block btn-primary" href="${pageContext.request.contextPath}/Cliente/Editar?id=${cli.codcliente}">Editar</a>
					</td>
					<td style="width:100px; height:45px;">
					<a class="btn btn-block btn-primary" href="${pageContext.request.contextPath}/Cliente/Eliminar?id=${cli.codcliente}">Eliminar</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
	</div>
	</div>
    </section>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetFooter.jsp"></jsp:include>     
