<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ page session="false" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="/WEB-INF/views/Includes/MasterIntranetHeader.jsp"></jsp:include>  
	<spring:url value="/resources/plantilla/bootstrap/css/dataTables.bootstrap.min.css" var="dataTablesCSS" />
	<!-- <link href="${dataTablesCSS}" rel="stylesheet" /> -->
	<section class="content-header">
		<h1>
			Venta de Productos <small><a class="box-subtitle" href="${pageContext.request.contextPath}/Menu">Menu</a></small>
		</h1> 
	</section>
 
   <section class="content">
   <form id="customerForm" method="POST">	    
   <div class="box box-info">     
       <div class="box-header with-border">
       	<label>Cliente</label>	
         <div class="input-group">
		      <div class="input-group-btn">
		        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#buscarCliente" aria-haspopup="true" aria-expanded="false">
		          Buscar Cliente
		        </button>
		      </div>
		      <input type="text" class="form-control" disabled id="txtnombreCliente">
	      </div>
	      <label>Impuesto</label>	
	      <div class="input-group">
		      <input type="text" class="form-control" disabled value="18.00">
		      <span class="input-group-btn">
		        <button class="btn btn-warning" type="button">%</button>
		      </span>
	      </div>
	      <br/>
     	  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#buscarProducto">
         	Buscar Producto
         </button>
   	   </div>
       <div class="box-header with-border">
			<%
			if(request.getParameter("msj")!=null)
			out.print("<p>"+request.getParameter("msj")!=null?request.getParameter("msj"):""+"</p>");
			%>
   	   </div>
   	   <div class="box-body">
	   	   <!--  -->
	   	   <input type="hidden" id="txtidcliente"class="form-control" placeholder="Id Cliente"/>
		   <input type="hidden" id="txtidProducto"class="form-control" placeholder="Id Producto"/>
		  <!-- <input type="text" id="txtcantidad" style="margin-left:10px;" class="form-control" placeholder="Cantidad"/> -->
	   	   <div class="row">
		   	     <div class="col-lg-3 col-xs-3">	        
		          		  <select class="form-control" id="cboTipoDocumento">
		          		  		<option value="0">Seleccione el Tipo de Documento</option>
		                 		<option value="BOLETA">Boleta</option>
		                 		<option value="FACTURA">Factura</option>
	                      </select>
		   	    </div>
		   	    <div class="col-lg-3 col-xs-3">	        
		          		 <input type="date" class="form-control" id="txtFechaVenta">
		   	    </div>
	   	   </div>
	   	   <br/>      
			<div class="table-responsive">
			<!-- codproducto nombre descripcion cantidad preciov valor_venta -->
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>							
							<th>Producto</th>
							<th>Descripcion</th>
							<th>Categoria</th>
							<th>Precio</th>
							<th>Cantidad</th>
							<th>Sub Total</th>
							<th>Eliminar</th>
						</tr>
					</thead>
					<tbody id="listaDetalleVenta">

					</tbody>
				</table>
			</div> 
			<div class="row">
				<div class="col-lg-3 col-xs-3 col-md-offset-1">	        
		        	<div class="input-group">
					    <span class="input-group-btn">
					       <button class="btn btn-warning" type="button" id="btnSubTotal">SUB-TOTAL:</button>
					    </span>
					    <input type="text" class="form-control" id="txtSubTotal" disabled placeholder="Sub-Total">					    
				    </div>  		 
		   	    </div>
		   	    <div class="col-lg-3 col-xs-3">	        
		        	<div class="input-group">
					    <span class="input-group-btn">
					       <button class="btn btn-warning" type="button" id="txtIgv">S/. I.G.V 18%:</button>
					    </span>
					    <input type="text" class="form-control" disabled placeholder="I.G.V">					    
				    </div>  		 
		   	    </div>
		   	    <div class="col-lg-3 col-xs-3">	        
		        	<div class="input-group">
					    <span class="input-group-btn">
					       <button class="btn btn-warning" type="button">S/. TOTAL:</button>
					    </span>
					    <input type="text" class="form-control" disabled placeholder="Total" id="txtTotal">					    
				    </div>  		 
		   	    </div>
			</div>
			<br/>
	   	   <button type="submit" class="btn btn-info">Generar Venta</button> 
		</div>	
		<div class="box-footer">
			<div id="postResultDiv"></div>
		</div>	
	</div>
	<div class="modal fade" id="buscarCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					
				</div>
				<div class="modal-body">
					   <div class="box box-info">
				       <div class="box-header with-border">
				     	  <a class="box-title">Buscar Cliente</a>
				   	   </div>				    
				
					 <div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="mydataCliente">
						<thead>
							<tr>	
							<!-- codcliente nombre ape_pater ape_mater direccion telefono -->	
								<th>Seleccionar</th>						
								<th>nombre</th>
								<th>ape_pater</th>
								<th>ape_mater</th>
								<th>direccion</th>
								<th>telefono</th>
							</tr>
						</thead>
						<tbody id="lista">
							
						</tbody>
					</table>
					</div>
					</div>
				</div>
				<div class="modal-footer">
					<div id="mensaje"></div>
					<button type="button" class="btn btn-info" id="btnAgredarCliente">Agregar</button> 
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="buscarProducto" tabindex="-1" role="dialog" aria-labelledby="ProModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					
				</div>
				<div class="modal-body">
					   <div class="box box-info">
				       <div class="box-header with-border">
				     	  <a class="box-title">Buscar Producto</a>
				   	   </div>				    
				
					 <div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="mydataProducto">
						<thead>
							<tr>	
								<th>Selecccionar</th>				
								<th>Producto</th>
								<th>Descripcion</th>
								<th>Categoria</th>
								<th>Precio</th>
								<th>Unidades en Existencia</th>
							</tr>
						</thead>
						<tbody id="listaPro">
							
						</tbody>
					</table>
					</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info" id="btnAgredarProducto">Agregar</button> 
				</div>
			</div>
		</div>
	</div>
	</form>
	
    </section>
    <spring:url value="/resources/js/getVentaRequest.js" var="getVentaRequest" />			
	<script src="${getVentaRequest}" type="text/javascript"></script>
	<spring:url value="/resources/js/postVentaRequest.js" var="postVentaRequest" />			
	<script src="${postVentaRequest}" type="text/javascript"></script>
	<spring:url value="/resources/plantilla/bootstrap/js/dataTables.bootstrap.min.js" var="dataTablesJS" />			
	<!-- <script src="${dataTablesJS}" type="text/javascript"></script> -->

<jsp:include page="/WEB-INF/views/Includes/MasterIntranetFooter.jsp"></jsp:include>     
