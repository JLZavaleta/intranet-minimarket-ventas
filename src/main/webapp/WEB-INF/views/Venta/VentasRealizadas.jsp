<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ page session="false" %>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetHeader.jsp"></jsp:include>  
	<section class="content-header">
		<h1>
			Ventas Realizadas <small><a class="box-subtitle" href="${pageContext.request.contextPath}/Menu">Menu</a></small>
		</h1> 
	</section>
 
   <section class="content">
	    
   <div class="box box-info">
       <div class="box-header with-border">
     	  <a class="box-title" href="#">Lista de Ventas</a>
   	   </div>
    
       <div class="box-header with-border">
			<%
			if(request.getParameter("msj")!=null)
			out.print("<p>"+request.getParameter("msj")!=null?request.getParameter("msj"):""+"</p>");
			%>
   	   </div>  
  <div class="box-body">	
	 <div class="table-responsive">
	<table class="table table-striped">
		<thead>
			<tr>
			 	
				<th></th>	 						
				<th>Tipo documento</th>
				<th>Fecha de Emision</th>
				<th>Dni</th>
				<th>Nombre Cliente</th>
				<th>Apellidos</th>				
			</tr>
		</thead>
		<tbody>
		<!-- v.numdocventa, v.codcliente, v.tipodoc, v.f_emision,c.nombre, c.ape_pater, c.ape_mater, c.dni -->
			<c:forEach items="${ventasRealizadas}" var="ventas">
				<tr style="">	
					<td style="width:100px; height:45px;">
					<a class="btn btn-block btn-primary" target="_blank" href="${pageContext.request.contextPath}/Venta/ventas/pdf?id=${ventas.numdocventa}">Ver PDF</a>
					</td>
					<td>${ventas.tipodoc}</td>
					<td>${ventas.f_emision}</td>
					<td>${ventas.cliente.dni}</td>
					<td>${ventas.cliente.nombre}</td>
					<td>${ventas.cliente.ape_pater} ${ventas.cliente.ape_mater}</td>	
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
	</div>
	</div>
    </section>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetFooter.jsp"></jsp:include>   