<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="false" %>
<spring:url value="/resources/Imagenes/default-user.png" var="user"/>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetHeader.jsp"></jsp:include>  
	<section class="content-header">
		<h1>
			Lista de los 10 Trabajadores con mas ventas<small><a class="box-subtitle" href="${pageContext.request.contextPath}/Menu">Menu</a></small>
		</h1> 
	</section>
 
   <section class="content">
	    
   <div class="box box-info">
       <div class="box-header with-border">
     	  <a class="box-title" target="_blank" href="${pageContext.request.contextPath}/Reportes/pdf/trabajador/ventas">PDF</a>
   	   </div>
    
       <div class="box-header with-border">
			<%
			if(request.getParameter("msj")!=null)
			out.print("<p>"+request.getParameter("msj")!=null?request.getParameter("msj"):""+"</p>");
			%>
   	   </div>  
  <div class="box-body">	  
		<div class="row">	
		    <c:forEach items="${listatrabajadoresConMasVentas}" var="tra">
			 <div class="col-sm-6 col-md-4">
			    <div class="thumbnail">
			      <img src="${user}" width="190" height="200" alt="user">
			      <div class="caption">
			      	<h3>USUARIO: ${tra.usuario}</h3>
			        <h4>NOMBRE: ${tra.nombre}</h4>			        
			        <h4>APELLIDOS: ${tra.ape_pater} ${tra.ape_mater}</h4>
			        <h4>TELEFONO: ${tra.telefono}</h4>
			        <h4>DIRECCION: ${tra.direccion}</h4>
			        <p>Cantidad de Productos Vendidos: ${tra.cantidad}</p>
			     </div>
			    </div>
			 </div>
			</c:forEach>
		</div>
	</div>
	</div>
    </section>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetFooter.jsp"></jsp:include>    