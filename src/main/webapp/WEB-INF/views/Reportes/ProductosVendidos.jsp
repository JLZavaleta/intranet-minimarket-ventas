<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="false" %>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetHeader.jsp"></jsp:include>  
	<section class="content-header">
		<h1>
			Lista Productos<small><a class="box-subtitle" href="${pageContext.request.contextPath}/Menu">Menu</a></small>
		</h1> 
	</section>
 
   <section class="content">
	    
   <div class="box box-info">
       <div class="box-header with-border">
     	  <a class="box-title" href="#">Productos mas Vendidos</a>
   	   </div>
    
       <div class="box-header with-border">
			<form id="FormListProductosMasVendidos" method="POST">			 
			 <div class="form-group">
			 <div class="row">
                  <label for="inputPassword3" class="col-sm-2 col-lg-2  control-label">Fecha Desde</label>
                  <div class="col-lg-3 col-xs-3">
                  	<input class="form-control" type="date" placeholder="fecha Desde" id="txtfechadesde"/>
                  </div>
                  <label for="inputPassword3" class="col-sm-1 control-label">fecha Hasta</label>
                  <div class="col-lg-3 col-xs-3">
                  	<input class="form-control" type="date" placeholder="fecha Hasta" id="txtfechahasta"/>
                  </div>
                  <label for="inputPassword3" class="col-sm-1 control-label">Limite</label>
                  <div class="col-lg-2 col-xs-2">
                  	<input type="text" class="form-control" placeholder="limite" id="txtlimite"/>
                  </div>
            </div>      
            </div>  
            <div class="form-group">
                 	<button type="submit" class="btn btn-info pull-right">Filtrar Datos</button> 
            </div>                  
	   	     
		   </form>
			
   	   </div>  
  <div class="box-body">	
	 <div class="table-responsive">
	<table class="table table-striped">
		<thead>
			<tr>								
				<th>Producto</th>
				<th>Descripcion</th>
				<th>Categoria</th>
				<th>Precio</th>
				<th>Unidades en Existencia</th>
			</tr>
		</thead>
		<tbody id="listaProductosMasVendidos">
		</tbody>
	</table>
	</div>
	</div>
	</div>
    </section>
    <spring:url value="/resources/js/postProductosMasVendidos.js" var="postProductosMasVendidos" />			
	<script src="${postProductosMasVendidos}" type="text/javascript"></script>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetFooter.jsp"></jsp:include>  