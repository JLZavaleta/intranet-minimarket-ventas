<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="false" %>
<spring:url value="/resources/Imagenes/default-user.png" var="user"/>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetHeader.jsp"></jsp:include>  
	<section class="content-header">
		<h1>
			Lista de Ventas Realizadas por cada Trabajador<small><a class="box-subtitle" href="${pageContext.request.contextPath}/Menu">Menu</a></small>
		</h1> 
	</section>
 
   <section class="content">
	    
   <div class="box box-info">
       <div class="box-header with-border">
     	  <a class="box-title" target="_blank" href="${pageContext.request.contextPath}/Reportes/pdf/ventasRealizada/trabajado">PDF</a>
   	   </div>
    
       <div class="box-header with-border">
			<%
			if(request.getParameter("msj")!=null)
			out.print("<p>"+request.getParameter("msj")!=null?request.getParameter("msj"):""+"</p>");
			%>
   	   </div>  
  <div class="box-body">	
  
	  <div class="panel panel-default">
		  <!-- Default panel contents -->
		<c:forEach items="${listaventasRealizadasPorTrabajador}" var="obj">
		  <div class="panel-heading"><strong>${obj.trabajador.nombre} ${obj.trabajador.ape_pater} ${obj.trabajador.ape_mater}</strong></div>
		  <div class="panel-body">
		  	<h5><strong>Usuario:</strong> ${obj.trabajador.usuario}</h5>
		  	<h5><strong>Fecha de </strong>Emision: ${obj.f_emision}</h5>
		  	<div class="panel panel-default">
			  <div class="panel-body">
			    <strong>Informacion del cliente:</strong>
			  </div>
			</div>
		  	<table class="table table-striped">
		  	<thead>
			<tr>
		  		<th>Dni</th>
		  		<th>Nombre</th>
		  		<th>Apellidos</th>
		  		<th>Direccion</th>
		  		<th>Telefono</th>
		  	</tr>
			</thead>	
			<tbody>
				<tr>
					<td>${obj.cliente.dni}</td>
					<td>${obj.cliente.nombre}</td>
					<td>${obj.cliente.ape_pater} ${obj.cliente.ape_mater}</td>
					<td>${obj.cliente.direccion}</td>
					<td>${obj.cliente.telefono}</td>
				</tr>
			</tbody>	  		
		  	</table>
		  	<!--  -->
		  	<div class="panel panel-default">
			  <div class="panel-body">
			    <strong>Informacion del Producto:</strong>
			  </div>
			</div>
		  	<table class="table table-striped">
		  	<thead>
			<tr>
		  		<th>Producto</th>
		  		<th>Descripcion</th>
		  		<th>Categoria</th>
		  		<th>Precio</th>
		  		<th>Cantidad de Productos</th>
		  	</tr>
			</thead>	
			<tbody>
				<tr>
					<td>${obj.dt.producto.nombre}</td>
					<td>${obj.dt.producto.descripcion}</td>
					<td>${obj.dt.producto.categoria.descripcion}</td>
					<td>${obj.dt.producto.preciov}</td>
					<td>${obj.dt.cantidad}</td>
				</tr>
			</tbody>	  		
		  	</table>		    
		  </div>
		</c:forEach>
	  </div>
	</div>
	</div>
    </section>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetFooter.jsp"></jsp:include>    