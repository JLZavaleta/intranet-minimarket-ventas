<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ page session="false" %>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetHeader.jsp"></jsp:include>  
	<section class="content-header">
		<h1>
			Productos con Stock Bajo<small><a class="box-subtitle" href="${pageContext.request.contextPath}/Menu">Menu</a></small>
		</h1> 
	</section>
 
   <section class="content">
	    
   <div class="box box-info">
       <div class="box-header with-border">
     	  <a class="box-title" target="_blank" href="${pageContext.request.contextPath}/Reportes/pdf/productos/bajoStock">PDF</a>
   	   </div>
    
       <div class="box-header with-border">
			<%
			if(request.getParameter("msj")!=null)
			out.print("<p>"+request.getParameter("msj")!=null?request.getParameter("msj"):""+"</p>");
			%>
   	   </div>  
  <div class="box-body">	
	 <div class="table-responsive">
	<table class="table table-striped">
		<thead>
			<tr>								
				<th>Producto</th>
				<th>Descripcion</th>
				<th>Categoria</th>
				<th>Precio</th>
				<th>Unidades en Existencia</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listaProductosConBajoStock}" var="prod">
				<tr style="">					
					<td>${prod.nombre}</td>
					<td>${prod.descripcion}</td>
					<td>${prod.categoria.descripcion}</td>
					<td>${prod.preciov}</td>
					<td>${prod.stock}</td>	
					<td style="width:100px; height:45px;">
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
	</div>
	</div>
    </section>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetFooter.jsp"></jsp:include>  