<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetHeader.jsp"></jsp:include>  

 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bienvenido
        <small>Todo comienza aqu�</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

	    		<!-- Fecha--->
				    <div class="wrap">
				        <div class="widget">
				            <div class="fecha">
				                <p id="diaSemana" class="diaSemana"></p>
				                <p id="dia" class="dia"></p>
				                <p>de</p>
				                <p id="mes" class="mes"></p>
				                <p>del</p>
				                <p id="year" class="year"></p>
				            </div>
				
				            <div class="reloj">
				                <p id="horas" class="horas"></p>
				                <p>:</p>
				                <p id="minutos" class="minutos"></p>
				                <p>:</p>
				                <div class="caja-segundos">
				                    <p id="ampm" class="ampm"></p>
				                    <p id="segundos" class="segundos"></p>
				                </div>
				            </div>
				        </div>
				    </div>
				
				<!-- Fecha fin--->

	    <!-- /.box -->
    </section>
    <!-- /.content -->
    <spring:url value="/resources/js/reloj/reloj.js" var="relojjs" />
    <script src="${relojjs}" type="text/javascript"></script>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetFooter.jsp"></jsp:include>       