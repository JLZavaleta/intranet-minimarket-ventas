<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ page session="false" %>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetHeader.jsp"></jsp:include>  
	<section class="content-header">
		<h1>
			Lista de Trabajadores <small><a class="box-subtitle" href="${pageContext.request.contextPath}/Menu">Menu</a></small>
		</h1> 
	</section>
 
   <section class="content">
	    
   <div class="box box-info">
       <div class="box-header with-border">
     	  <a class="box-title" href="${pageContext.request.contextPath}/Trabajador/Nuevo">Nuevo Trabajador</a>
   	   </div>
    
       <div class="box-header with-border">
			<%
			if(request.getParameter("msj")!=null)
			out.print("<p>"+request.getParameter("msj")!=null?request.getParameter("msj"):""+"</p>");
			%>
   	   </div>  
  <div class="box-body">	
	 <div class="table-responsive">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Nombre</th>			
				<th>Ape_pater</th>
				<th>Ape_mater</th>
				<th>Direccion</th>
				<th>Telefono</th>
				<th>Usuario</th>
				<th>Passwd</th>
				<th>Es Administrador</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listaTrabajadores}" var="trab">
				<tr style="">
					<td>${trab.nombre}</td>					
					<td>${trab.ape_pater}</td>
					<td>${trab.ape_mater}</td>
					<td>${trab.direccion}</td>
					<td>${trab.telefono}</td>
					<td>${trab.usuario}</td>
					<td>${trab.passwd}</td>
					<td>${trab.tipo}</td>	
					<td style="width:100px; height:45px;">
					<a class="btn btn-block btn-primary" href="${pageContext.request.contextPath}/Trabajador/Editar?id=${trab.codtrabajador}">Editar</a>
					</td>
					<td style="width:100px; height:45px;">
					<a class="btn btn-block btn-primary" href="${pageContext.request.contextPath}/Trabajador/Eliminar?id=${trab.codtrabajador}">Eliminar</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
	</div>
	</div>
    </section>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetFooter.jsp"></jsp:include>     
