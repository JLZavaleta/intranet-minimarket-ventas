<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ page session="false" %>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetHeader.jsp"></jsp:include>  
	<section class="content-header">
		<h1>
			Eliminar Trabajador <small>Eliminar Datos del Trabajador</small>
		</h1> 
	</section>
	<section class="content">
	
	<div class="box box-info">
         <div class="box-header with-border">  
     		<a class="box-title" href="${pageContext.request.contextPath}/Trabajador/lista">Lista Trabajadores</a>
     	</div>
	<frm:form cssClass="form-horizontal" method="POST" action="GuardarEliminar">
              <div class="box-body">
                <div class="form-group">
                  <div class="col-sm-10">              
                    <frm:hidden path="codtrabajador"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Nombre Trabajador</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" disabled="true" placeholder="Nombre Trabajador" path="nombre"></frm:input>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Apellido Paterno</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" disabled="true" placeholder="Apellido Paterno" path="ape_pater"></frm:input>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Apellido materno</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" disabled="true" placeholder="Apellido materno" path="ape_mater"></frm:input>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Direccion</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" disabled="true" placeholder="Direccion" path="direccion"></frm:input>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Telefono</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" disabled="true" placeholder="Telefono" path="telefono"></frm:input>
                  </div>
                </div>
                <!-- codtrabajador, nombre, ape_pater, ape_mater, direccion, telefono, usuario, passwd, tipo  -->
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Usuario</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" disabled="true" placeholder="Usuario" path="usuario"></frm:input>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                  <div class="col-sm-10">
                  	<frm:input cssClass="form-control" disabled="true" placeholder="password" path="passwd"></frm:input>
                  </div>
                </div>
              </div>
			<!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" class="btn btn-info pull-right" value="Eliminar"/>
              </div>
            <!-- /.box-footer -->
	</frm:form>
	</div>
	</section>
<jsp:include page="/WEB-INF/views/Includes/MasterIntranetFooter.jsp"></jsp:include>  