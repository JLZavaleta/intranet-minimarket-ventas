<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <spring:url value="/resources/plantilla/bootstrap/css/bootstrap.min.css" var="bootstrapcss"/>
  
  <spring:url value="/resources/plantilla/dist/css/AdminLTE.min.css" var="AdminLTEcss"/>
  <spring:url value="/resources/plantilla/plugins/iCheck/square/blue.css" var="bluecss"/>
  <spring:url value="/resources/plantilla/plugins/jQuery/jquery-3.1.1.min.js" var="jquery"/>
  <spring:url value="/resources/plantilla/bootstrap/js/bootstrap.min.js" var="bootstrap"/>
  <spring:url value="/resources/plantilla/plugins/iCheck/icheck.min.js" var="icheck"/>
  <title>Intranet MiniMarket | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${bootstrapcss}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${AdminLTEcss}">
  <!-- iCheck -->
  <link rel="stylesheet" href="${bluecss}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Intranet</b>MiniMarket</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Inicia sesi�n para comenzar</p>

    <frm:form method="POST" action="VerificarAcceso">
      <div class="form-group has-feedback">
        <frm:input type="text" cssClass="form-control" path="usuario" placeholder="usuario"></frm:input>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <frm:input type="password" cssClass="form-control" path="passwd" placeholder="password"></frm:input>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <frm:checkbox items="Admin"  path="tipo"/> Administrador
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <input type="submit" class="btn btn-primary btn-block btn-flat" value="Ingresar"/>
        </div>
        <!-- /.col -->
      </div>
      <h4>	${mensaje} </h4>
    </frm:form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3.1.1 -->
<script src="${jquery}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${bootstrap}"></script>
<!-- iCheck -->
<script src="${icheck}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>

