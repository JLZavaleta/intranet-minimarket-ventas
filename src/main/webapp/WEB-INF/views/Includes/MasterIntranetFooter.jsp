<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

			</section>
		</div>
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0.0
			</div>
			<strong>Copyright &copy; 2018 <a
				href="#">Proyecto Integrador II - Intranet MiniMarket</a>.
			</strong> Todos los derechos reservados.
		</footer>
	</div>
	<spring:url value="/resources/plantilla/bootstrap/js/bootstrap.min.js" var="bootstrapminJS" />
	<spring:url value="/resources/plantilla/plugins/datatables/jquery.dataTables.min.js" var="dataTablesJS"/>
	<spring:url value="/resources/plantilla/plugins/datatables/dataTables.bootstrap.min.js" var="dataTablesbootstrapJS"/>	
	<spring:url value="/resources/plantilla/plugins/slimScroll/jquery.slimscroll.min.js" var="slimscrollJS" />
	<spring:url value="/resources/plantilla/plugins/fastclick/fastclick.js" var="fastclickJS" />
	<spring:url value="/resources/plantilla/dist/js/adminlte.min.js" var="adminlteJS" />
	<spring:url value="/resources/plantilla/dist/js/demo.js" var="demoJS" />
	<spring:url value="/resources/plantilla/plugins/iCheck/icheck.min.js" var="icheck" />

	<spring:url value="/resources/plantilla/plugins/datepicker/bootstrap-datepicker.js" var="datepicker" />
	<spring:url value="/resources/plantilla/plugins/colorpicker/bootstrap-colorpicker.min.js" var="colorpicker" />
	<spring:url value="/resources/plantilla/plugins/timepicker/bootstrap-timepicker.min.js" var="timepicker" />
			
	<script src="${bootstrapminJS}" type="text/javascript"></script>
	<script src="${dataTablesJS}" type="text/javascript"></script> 
	 <script src="${dataTablesbootstrapJS}" type="text/javascript"></script>	
	<script src="${slimscrollJS}" type="text/javascript"></script>
	<script src="${fastclickJS}" type="text/javascript"></script>
	<script src="${adminlteJS}" type="text/javascript"></script>
	<script src="${demoJS}" type="text/javascript"></script>
	<script src="${relojjs}" type="text/javascript"></script>
	<script src="${icheck}" type="text/javascript"></script>

	<script src="${datepicker}" type="text/javascript"></script>
	<script src="${colorpicker}" type="text/javascript"></script>
	<script src="${timepicker}" type="text/javascript"></script>
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
  
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
  
</script>
</body>
</html>