<%@page import="com.intranet.minimarket.ventas.entidades.entTrabajador"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Intranet | Carrito Compras</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<spring:url value="/resources/plantilla/bootstrap/css/bootstrap.min.css" var="bootstrapMin" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<spring:url value="/resources/plantilla/dist/css/AdminLTE.min.css" var="adminLTE" />
<spring:url value="/resources/plantilla/dist/css/skins/_all-skins.min.css" var="allSkins" />
<spring:url value="/resources/plantilla/plugins/datatables/dataTables.bootstrap.css" var="dataTables"/>
<spring:url value="/resources/plantilla/plugins/daterangepicker/daterangepicker.css" var="daterangepicker"/>
<spring:url value="/resources/plantilla/plugins/datepicker/datepicker3.css" var="datepicker3"/>
<spring:url value="/resources/plantilla/plugins/colorpicker/bootstrap-colorpicker.min.css" var="colorpicker"/>
<spring:url value="/resources/plantilla/plugins/timepicker/bootstrap-timepicker.min.css" var="timepicker"/>
<spring:url value="/resources/plantilla/plugins/iCheck/square/blue.css" var="blue"/>
<spring:url value="/resources/plantilla/dist/img/avatar5.png" var="avatar"/>
<spring:url value="/resources/plantilla/bootstrap/css/reloj/reloj.css" var="reloj"/>
<spring:url value="/resources/plantilla/plugins/jQuery/jquery-2.2.3.min.js" var="jQuery31JS" />

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link href="${bootstrapMin}" rel="stylesheet" />
<link href="${adminLTE}" rel="stylesheet" />
<link href="${allSkins}" rel="stylesheet" />
 <link href="${dataTables}" rel="stylesheet" /> 
<link href="${daterangepicker}" rel="stylesheet" />
<link href="${datepicker3}" rel="stylesheet" />
<link href="${colorpicker}" rel="stylesheet" />
<link href="${timepicker}" rel="stylesheet" />
<link href="${blue}" rel="stylesheet" />
<link href="${reloj}" rel="stylesheet" />
<script src="${jQuery31JS}" type="text/javascript"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<!-- Logo -->
			<a href="${pageContext.request.contextPath}/Menu" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>I</b>NT</span> <!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Intranet</b> MiniMarket</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li class="dropdown user user-menu">
							<%
								if(request.getSession().getAttribute("trabajador")!=null){
									entTrabajador e = (entTrabajador)request.getSession().getAttribute("trabajador");
							%>						
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
								<img src="${avatar}" class="user-image" alt="User Image"> 
								<span class="hidden-xs"><%=e.getNombre()+ " " 
														+ e.getApe_pater()%></span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-header">
									<img src="${avatar}" class="img-circle" alt="User Image">
									<p>
										<%=e.getNombre() + " " + e.getApe_pater()%> - Administrador <!-- <small>Member since Nov. 2012</small> -->
									</p></li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<!-- <a href="#" class="btn btn-default btn-flat">Perfil</a> -->
									</div>
									<div class="pull-right">
										<a href="${pageContext.request.contextPath}/CerrarSesion" class="btn btn-default btn-flat">Cerrar Sesion</a>
									</div>
								</li>
							</ul>
						</li>
						<%} %>
					</ul>
				</div>
			</nav>
		</header>
		<aside class="main-sidebar">			
			<section class="sidebar">
				<ul class="sidebar-menu" data-widget="tree">
					<li class="header">MENU PRINCIPAL</li>
					<li class="treeview">
						<a href="#"> <i
								class="fa fa-dashboard"></i> <span>Mantenedores</span> <span
								class="pull-right-container"> <i
									class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<!--
							<li>
								<a href="${pageContext.request.contextPath}/Empleado/listar"><i class="fa fa-circle-o"></i>Empleados</a>
							</li>-->
							<%
								if(request.getSession().getAttribute("trabajador")!=null){
									entTrabajador e = (entTrabajador)request.getSession().getAttribute("trabajador");
									if(e.isTipo()==true){
							%>	
							<li>
								<a href="${pageContext.request.contextPath}/Trabajador/lista"><i class="fa fa-circle-o"></i>Trabajador</a>
							</li>
							<%
								}}
							%>	
							<li>
								<a href="${pageContext.request.contextPath}/Cliente/lista"><i class="fa fa-circle-o"></i>Cliente</a>
							</li> 
							
							<li>
								<a href="${pageContext.request.contextPath}/Producto/lista"><i class="fa fa-circle-o"></i>Producto</a>
							</li>
						</ul>
					</li>	
					<li class="treeview">
						<a href="#"> <i
								class="fa fa-dashboard"></i> <span>Ventas</span> <span
								class="pull-right-container"> <i
									class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li>
								<a href="${pageContext.request.contextPath}/Venta/venta"><i class="fa fa-circle-o"></i>Realizar Venta</a>
							</li> 
							<li>
								<a href="${pageContext.request.contextPath}/Venta/ventas/realizadas"><i class="fa fa-circle-o"></i>Ventas Realizadas</a>
							</li> 							
						</ul>
					</li>	
					<li class="treeview">
						<a href="#"> <i
								class="fa fa-dashboard"></i> <span>Reportes</span> <span
								class="pull-right-container"> <i
									class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<%
								if(request.getSession().getAttribute("trabajador")!=null){
									entTrabajador e = (entTrabajador)request.getSession().getAttribute("trabajador");
									if(e.isTipo()==true){
							%>	
							<li>
								<a href="${pageContext.request.contextPath}/Reportes/clientes/habituales"><i class="fa fa-circle-o"></i>Clientes Habituales</a>
							</li> 
							<li>
								<a href="${pageContext.request.contextPath}/Trabajador/lista/ventasRealizada/trabajador"><i class="fa fa-circle-o"></i>Trabajador Ventas Realizadas</a>
							</li> 
							<li>
								<a href="${pageContext.request.contextPath}/Trabajador/lista/trabajador/ventas"><i class="fa fa-circle-o"></i>Trabajador con mas Ventas </a>
							</li> 
							<%
								}}
							%>								
							<li>
								<a href="${pageContext.request.contextPath}/Reportes/productos/stockBajo"><i class="fa fa-circle-o"></i>Productos con Stock bajo</a>
							</li>
							<li>
								<!--<a href="${pageContext.request.contextPath}/Reportes/pdf/ventas"><i class="fa fa-circle-o"></i>Productos Vendidos</a> -->
							</li>
						</ul>
					</li>		
				</ul>
			</section>
		</aside>
		<div class="content-wrapper"> 
		<!-- 	<section class="content-header">
				<h1>
					Menu principal <small>Bienvenido a la intranet</small>
				</h1> 
			</section>
 
			<section class="content">  -->
