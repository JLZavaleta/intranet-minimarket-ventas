package com.intranet.minimarket.ventas.reportes;

import java.util.ArrayList;

import org.springframework.web.servlet.ModelAndView;

import com.intranet.minimarket.ventas.entidades.entProducto;
import com.intranet.minimarket.ventas.negocio.negReportes;

public class prueba {

	public static void main(String[] args) {
		ModelAndView m = null;
		try {
			int valor=1;
			int limite=0;
			String fechaInicio = null;
			String fechaFin = null;
			ArrayList<entProducto> lista = null;						
			switch (valor) {
			case 1:
				lista = negReportes.Instancia().productosMasVendidos();
				break;
			case 2:
				lista = negReportes.Instancia().productosMasVendidosLimite(limite);
				break;
			case 3:
				lista = negReportes.Instancia().productosMasVendidosFechas(fechaInicio, fechaFin);
				break;
			case 4:
				lista = negReportes.Instancia().productosMasVendidosFechasLimite(fechaInicio, fechaFin, limite);					
				break;
			default:
				break;
			}							
			if(lista!=null) {	
				m = new ModelAndView("pdfProductosVenta");
				m.addObject("listaProductosVenta",lista);
				m.addObject("valor", valor);
				m.addObject("limite",limite);
				m.addObject("fechaInicio", fechaInicio);
				m.addObject("fechaFin",fechaFin);				
			}else {
				m = new ModelAndView("frmError");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
	}

}
