package com.intranet.minimarket.ventas.reportes;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.intranet.minimarket.ventas.entidades.entTrabajador;
import com.intranet.minimarket.ventas.util.util;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFtrabajadoresConMasVentas extends AbstractITextPdfView{

	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		Font smallfont = new Font(FontFamily.HELVETICA, 10);	
		@SuppressWarnings("unchecked")
		ArrayList<entTrabajador> lista  = (ArrayList<entTrabajador>)model.get("listatrabajadoresConMasVentas");
		
		Image image = Image.getInstance(PDFProductosBajoStock.class.getResource(util.getImagenes().getString("logo")));
		//image.scaleToFit(96f, 96f);
		Paragraph logo = new Paragraph(10);
		logo.add(image);
		logo.setFont(smallfont);
		logo.add("   MiniMarket");
		logo.add(Chunk.NEWLINE);
		logo.add(Chunk.NEWLINE);
		logo.add(Chunk.NEWLINE);
		document.add(logo);
		//c.codcliente, c.nombre, c.ape_pater, c.ape_mater, c.dni, c.direccion, c.telefono,
		PdfPTable table = new PdfPTable(6);
        table.setWidths(new float[] {10, 10, 10, 10, 10,10});
        table.setWidthPercentage(100);
		
        PdfPCell cell_TRABAJADOR_USUARIO = new PdfPCell(new Phrase("USUARIO TRABAJADOR", smallfont));
        cell_TRABAJADOR_USUARIO.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell_TRABAJADOR_USUARIO.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cell_TRABAJADOR_NOMBRE = new PdfPCell(new Phrase("NOMBRE TRABAJADOR", smallfont));
        cell_TRABAJADOR_NOMBRE.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell_TRABAJADOR_NOMBRE.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cell_TRABAJADOR_APELLIDOS = new PdfPCell(new Phrase("APELLIDOS TRABAJADOR", smallfont));
        cell_TRABAJADOR_APELLIDOS.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell_TRABAJADOR_APELLIDOS.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cell_TRABAJADOR_DIRECION = new PdfPCell(new Phrase("DIRECION TRABAJADOR", smallfont));
        cell_TRABAJADOR_DIRECION.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell_TRABAJADOR_DIRECION.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cell_TRABAJADOR_TELEFONO = new PdfPCell(new Phrase("TELEFONO TRABAJADOR", smallfont));
        cell_TRABAJADOR_TELEFONO.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell_TRABAJADOR_TELEFONO.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cell_CANTIDAD = new PdfPCell(new Phrase("Cantidad de Productos Vendidos", smallfont));
        cell_CANTIDAD.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell_CANTIDAD.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        table.addCell(cell_TRABAJADOR_USUARIO); 
        table.addCell(cell_TRABAJADOR_NOMBRE); 
        table.addCell(cell_TRABAJADOR_APELLIDOS);
        table.addCell(cell_TRABAJADOR_DIRECION);
        table.addCell(cell_TRABAJADOR_TELEFONO);
        table.addCell(cell_CANTIDAD);
        
        
                  
        
        for(int i=0;i<lista.size();i++) {
            PdfPCell cell1 = new PdfPCell(new Phrase(lista.get(i).getUsuario(), smallfont));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            
            PdfPCell cell2 = new PdfPCell(new Phrase(lista.get(i).getNombre(), smallfont));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cell3 = new PdfPCell(new Phrase(lista.get(i).getApe_pater()+" "+lista.get(i).getApe_mater(), smallfont));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);

            PdfPCell cell4 = new PdfPCell(new Phrase(lista.get(i).getDireccion(), smallfont));
            cell4.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cell5 = new PdfPCell(new Phrase(lista.get(i).getTelefono(), smallfont));
            cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
            
            PdfPCell cell6 = new PdfPCell(new Phrase(String.valueOf(lista.get(i).getCantidad()), smallfont));
            cell6.setHorizontalAlignment(Element.ALIGN_CENTER);

	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        table.addCell(cell4);
	        table.addCell(cell5);
	        table.addCell(cell6);
        }
        document.add(table);		
	}

}
