package com.intranet.minimarket.ventas.reportes;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.intranet.minimarket.ventas.entidades.entVenta;
import com.intranet.minimarket.ventas.util.util;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFventasRealizadasPorTrabajador extends AbstractITextPdfView{

	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		Font smallfont = new Font(FontFamily.HELVETICA, 10);	
		@SuppressWarnings("unchecked")
		ArrayList<entVenta> lista  = (ArrayList<entVenta>)model.get("listaventasRealizadasPorTrabajador");
		
		Image image = Image.getInstance(PDFventasRealizadasPorTrabajador.class.getResource(util.getImagenes().getString("logo")));
		//image.scaleToFit(96f, 96f);
		Paragraph logo = new Paragraph(10);
		logo.add(image);
		logo.setFont(smallfont);
		logo.add("   MiniMarket");
		logo.add(Chunk.NEWLINE);
		logo.add(Chunk.NEWLINE);
		logo.add(Chunk.NEWLINE);
		document.add(logo);
				
		PdfPTable table = new PdfPTable(15);
        table.setWidths(new float[] {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10});
        table.setWidthPercentage(100);
		

        PdfPCell cell_TRABAJADOR_USUARIO = new PdfPCell(new Phrase("USUARIO TRABAJADOR", smallfont));
        cell_TRABAJADOR_USUARIO.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell_TRABAJADOR_USUARIO.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cell_TRABAJADOR_NOMBRE = new PdfPCell(new Phrase("NOMBRE TRABAJADOR", smallfont));
        cell_TRABAJADOR_NOMBRE.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell_TRABAJADOR_NOMBRE.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cell_TRABAJADOR_APELLIDOS = new PdfPCell(new Phrase("APELLIDOS TRABAJADOR", smallfont));
        cell_TRABAJADOR_APELLIDOS.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell_TRABAJADOR_APELLIDOS.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cell_TRABAJADOR_DIRECION = new PdfPCell(new Phrase("DIRECION TRABAJADOR", smallfont));
        cell_TRABAJADOR_DIRECION.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell_TRABAJADOR_DIRECION.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cell_TRABAJADOR_TELEFONO = new PdfPCell(new Phrase("TELEFONO TRABAJADOR", smallfont));
        cell_TRABAJADOR_TELEFONO.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell_TRABAJADOR_TELEFONO.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        /******************************************************/ 
        
        PdfPCell cellDNI = new PdfPCell(new Phrase("DNI CLIENTE", smallfont));
        cellDNI.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellDNI.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellNOMBRE = new PdfPCell(new Phrase("CLIENTE", smallfont));
        cellNOMBRE.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellNOMBRE.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellAPELLIDOS = new PdfPCell(new Phrase("APELLIDOS CLIENTE", smallfont));
        cellAPELLIDOS.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAPELLIDOS.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellDIRECION = new PdfPCell(new Phrase("DIRECION CLIENTE", smallfont));
        cellDIRECION.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellDIRECION.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellTELEFONO = new PdfPCell(new Phrase("TELEFONO CLIENTE", smallfont));
        cellTELEFONO.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellTELEFONO.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        /******************************************************/ 
        PdfPCell cellProducto = new PdfPCell(new Phrase("Producto", smallfont));
        cellProducto.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellProducto.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellDescripcion = new PdfPCell(new Phrase("Descripcion", smallfont));
        cellDescripcion.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellDescripcion.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellPrecio = new PdfPCell(new Phrase("Precio", smallfont));
        cellPrecio.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellPrecio.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellCantidad = new PdfPCell(new Phrase("Cantidad", smallfont));
        cellCantidad.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellCantidad.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellFecha = new PdfPCell(new Phrase("Fecha de Emision", smallfont));
        cellFecha.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellFecha.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        table.addCell(cell_TRABAJADOR_TELEFONO);
        table.addCell(cell_TRABAJADOR_DIRECION);
        table.addCell(cell_TRABAJADOR_APELLIDOS);
        table.addCell(cell_TRABAJADOR_NOMBRE);  
        table.addCell(cell_TRABAJADOR_USUARIO);  
        
        table.addCell(cellDNI);
        table.addCell(cellNOMBRE);
        table.addCell(cellAPELLIDOS);
        table.addCell(cellDIRECION);  
        table.addCell(cellTELEFONO);  
        
        table.addCell(cellProducto);
        table.addCell(cellDescripcion);        
        table.addCell(cellPrecio);  
        table.addCell(cellCantidad);  
        table.addCell(cellFecha);
        
        for(int i=0;i<lista.size();i++) {
        	
            PdfPCell cell1 = new PdfPCell(new Phrase(lista.get(i).getTrabajador().getUsuario(), smallfont));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            
            PdfPCell cell2 = new PdfPCell(new Phrase(lista.get(i).getTrabajador().getNombre(), smallfont));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cell3 = new PdfPCell(new Phrase(lista.get(i).getTrabajador().getApe_pater()+" "+lista.get(i).getCliente().getApe_mater(), smallfont));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);

            PdfPCell cell4 = new PdfPCell(new Phrase(lista.get(i).getTrabajador().getDireccion(), smallfont));
            cell4.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cell5 = new PdfPCell(new Phrase(lista.get(i).getTrabajador().getTelefono(), smallfont));
            cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
        	/************************************************************/
            PdfPCell cell6 = new PdfPCell(new Phrase(lista.get(i).getCliente().getDni(), smallfont));
            cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
            
            PdfPCell cell7 = new PdfPCell(new Phrase(lista.get(i).getCliente().getNombre(), smallfont));
            cell7.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cell8 = new PdfPCell(new Phrase(lista.get(i).getCliente().getApe_pater()+" "+lista.get(i).getCliente().getApe_mater(), smallfont));
            cell8.setHorizontalAlignment(Element.ALIGN_LEFT);

            PdfPCell cell9 = new PdfPCell(new Phrase(lista.get(i).getCliente().getDireccion(), smallfont));
            cell9.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cell10 = new PdfPCell(new Phrase(lista.get(i).getCliente().getTelefono(), smallfont));
            cell10.setHorizontalAlignment(Element.ALIGN_CENTER);
        	/************************************************************/
            PdfPCell cell11 = new PdfPCell(new Phrase(lista.get(i).getDt().getProducto().getNombre(), smallfont));
            cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
            
            PdfPCell cell12 = new PdfPCell(new Phrase(lista.get(i).getDt().getProducto().getDescripcion(), smallfont));
            cell12.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cell14 = new PdfPCell(new Phrase(lista.get(i).getDt().getProducto().getPreciov(), smallfont));
            cell14.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cell15 = new PdfPCell(new Phrase(String.valueOf(lista.get(i).getDt().getCantidad())));
            cell15.setHorizontalAlignment(Element.ALIGN_CENTER);
            
            PdfPCell cell13 = new PdfPCell(new Phrase(lista.get(i).getF_emision().toString(), smallfont));
            cell13.setHorizontalAlignment(Element.ALIGN_LEFT);

	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        table.addCell(cell4);
	        table.addCell(cell5);
	        
	        table.addCell(cell6);
	        table.addCell(cell7);
	        table.addCell(cell8);
	        table.addCell(cell9);
	        table.addCell(cell10);
	        
	        table.addCell(cell11);
	        table.addCell(cell12);	        
	        table.addCell(cell14);
	        table.addCell(cell15);
	        table.addCell(cell13);
        }
        document.add(table);		
		
	}

}
