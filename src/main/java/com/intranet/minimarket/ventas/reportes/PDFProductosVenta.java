package com.intranet.minimarket.ventas.reportes;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.intranet.minimarket.ventas.entidades.entProducto;
import com.intranet.minimarket.ventas.util.util;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFProductosVenta extends AbstractITextPdfView{

	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Font smallfont = new Font(FontFamily.HELVETICA, 10);	
		@SuppressWarnings("unchecked")
		ArrayList<entProducto> lista = (ArrayList<entProducto>)model.get("listaProductosVenta");
		
		int valor = (Integer)model.get("valor");
		int limite = (Integer)model.get("limite");
		Date fechaInicio = (Date)model.get("fechaInicio");
		Date fechaFin = (Date)model.get("fechaFin");
		
		Image image = Image.getInstance(PDFProductosVenta.class.getResource(util.getImagenes().getString("logo")));
		//image.scaleToFit(96f, 96f);
		Paragraph logo = new Paragraph(10);
		logo.add(image);
		logo.setFont(smallfont);
		logo.add("   MiniMarket");
		logo.add(Chunk.NEWLINE);
		logo.add(Chunk.NEWLINE);
		logo.add(Chunk.NEWLINE);
		document.add(logo);
		
		Paragraph titulo = new Paragraph(10);	
		titulo.setFont(smallfont);
			switch (valor) {
			case 1:
				titulo.add("LOS 10 PRODUCTOS MAS VENDIDOS");
				break;
			case 2:
				
				titulo.add("LOS "+limite+" PRODUCTOS MAS VENDIDOS");
				break;
			case 3:
				titulo.add("LOS 10 PRODUCTOS MAS VENDIDOS ENTRE: "+fechaInicio+" y "+fechaFin);
				break;
			case 4:
				titulo.add("LOS "+limite+" PRODUCTOS MAS VENDIDOS ENTRE: "+fechaInicio+" y "+fechaFin);
				break;
			default:
				titulo.add("PRODUCTOS MAS VENDIDOS");
				break;
			}			
			titulo.add(Chunk.NEWLINE);
			titulo.add(Chunk.NEWLINE);
			titulo.add(Chunk.NEWLINE);
			titulo.setAlignment(Chunk.ALIGN_CENTER);
			document.add(titulo);
		
		PdfPTable table = new PdfPTable(5);
        table.setWidths(new float[] {10, 10, 10, 10, 10});
        table.setWidthPercentage(100);
		
        PdfPCell cellNOMBRE = new PdfPCell(new Phrase("NOMBRE", smallfont));
        cellNOMBRE.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellNOMBRE.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellDESCRIPCION = new PdfPCell(new Phrase("DESCRIPCION", smallfont));
        cellDESCRIPCION.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellDESCRIPCION.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellCATEGORIA = new PdfPCell(new Phrase("CATEGORIA", smallfont));
        cellCATEGORIA.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellCATEGORIA.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellSTOCK = new PdfPCell(new Phrase("STOCK", smallfont));
        cellSTOCK.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellSTOCK.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellPRECIO = new PdfPCell(new Phrase("PRECIO", smallfont));
        cellPRECIO.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellPRECIO.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        table.addCell(cellNOMBRE);
        table.addCell(cellDESCRIPCION);
        table.addCell(cellCATEGORIA);
        table.addCell(cellSTOCK);  
        table.addCell(cellPRECIO);  
        
        for(int i=0;i<lista.size();i++) {
            PdfPCell cell1 = new PdfPCell(new Phrase(lista.get(i).getNombre(), smallfont));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            
            PdfPCell cell2 = new PdfPCell(new Phrase(lista.get(i).getDescripcion(), smallfont));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cell3 = new PdfPCell(new Phrase(lista.get(i).getCategoria().getDescripcion(), smallfont));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);

            PdfPCell cell4 = new PdfPCell(new Phrase(lista.get(i).getStock(), smallfont));
            cell4.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cell5 = new PdfPCell(new Phrase(lista.get(i).getPreciov(), smallfont));
            cell5.setHorizontalAlignment(Element.ALIGN_CENTER);

	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        table.addCell(cell4);
	        table.addCell(cell5);
        }
        document.add(table);
		
	}

}
