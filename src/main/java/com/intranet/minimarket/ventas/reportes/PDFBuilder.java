package com.intranet.minimarket.ventas.reportes;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.intranet.minimarket.ventas.entidades.entVenta;
import com.intranet.minimarket.ventas.util.util;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFBuilder extends AbstractITextPdfView{

	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
			Font smallfont = new Font(FontFamily.HELVETICA, 10);
			Font smallfont1 = new Font(FontFamily.HELVETICA, 8);
			
			entVenta venta = (entVenta)model.get("ventaMasDetalle");
			
			Image image = Image.getInstance(PDFProductosBajoStock.class.getResource(util.getImagenes().getString("logo")));
			//image.scaleToFit(96f, 96f);
			Paragraph logo = new Paragraph(10);
			logo.add(image);
			logo.setFont(smallfont);
			logo.add("   MiniMarket");
			logo.add(Chunk.NEWLINE);
			logo.add(Chunk.NEWLINE);
			logo.add(Chunk.NEWLINE);
			document.add(logo);
    		
    		Paragraph cliente = new Paragraph(10);
    		cliente.add("Cliente");
    		cliente.setFont(smallfont);
    		cliente.add(Chunk.NEWLINE);
    		document.add(cliente);
    		
    		PdfPTable table0 = new PdfPTable(3);
	        table0.setWidths(new float[] {4,30, 10});
	        table0.setWidthPercentage(100);	
	        
	        List listaCliente = new List();
	        listaCliente.setListSymbol("");
	        listaCliente.add(new ListItem("Dni",smallfont));
	        listaCliente.add(new ListItem("Nombre",smallfont));
	        listaCliente.add(new ListItem("Direccion",smallfont));
	        listaCliente.add(new ListItem("Telefono",smallfont));
	        
	        List listaCliente1 = new List();
	        listaCliente1.setListSymbol("");
	        listaCliente1.add(new ListItem(": "+venta.getCliente().getDni(),smallfont));
	        listaCliente1.add(new ListItem(": "+venta.getCliente().getNombre()+" "+venta.getCliente().getApe_pater()+" "+venta.getCliente().getApe_mater(),smallfont));
	        listaCliente1.add(new ListItem(": "+venta.getCliente().getDireccion(),smallfont));
	        listaCliente1.add(new ListItem(": "+venta.getCliente().getTelefono(),smallfont));
	        
	        
            Paragraph p = new Paragraph(10);
            p.setFont(smallfont);
            p.add(Chunk.NEWLINE);
            p.add(venta.getTipodoc()+" DE VENTA");
            p.add(Chunk.NEWLINE);
            p.add(Chunk.NEWLINE);
            p.add("Fecha: "+venta.getF_emision());
            p.setAlignment(Chunk.ALIGN_CENTER);

	        PdfPCell infoVenta = new PdfPCell();
	        infoVenta.addElement(listaCliente);
	        infoVenta.setBorder(0);
	        
	        PdfPCell infoVentaData = new PdfPCell();
	        infoVentaData.addElement(listaCliente1);
	        infoVentaData.setBorder(0);
	        
	        PdfPCell fechayTipo = new PdfPCell();
	        fechayTipo.addElement(p);
	        	        
	        table0.addCell(infoVenta);
	        table0.addCell(infoVentaData);
	        table0.addCell(fechayTipo);	        
	        document.add(table0);
	        document.add(Chunk.NEWLINE);
	            		  	    
    		PdfPTable table = new PdfPTable(4);
	        table.setWidths(new float[] {10, 30, 10, 10});
	        table.setWidthPercentage(100);

	        
            PdfPCell cellCANTIDAD = new PdfPCell(new Phrase("CANTIDAD", smallfont));
            cellCANTIDAD.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellCANTIDAD.setBackgroundColor(BaseColor.LIGHT_GRAY);
            
            PdfPCell cellPRODUCTO = new PdfPCell(new Phrase("PRODUCTO", smallfont));
            cellPRODUCTO.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellPRODUCTO.setBackgroundColor(BaseColor.LIGHT_GRAY);
            
            PdfPCell cellPRECIO = new PdfPCell(new Phrase("PRECIO", smallfont));
            cellPRECIO.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellPRECIO.setBackgroundColor(BaseColor.LIGHT_GRAY);
            
            PdfPCell cellTOTAL = new PdfPCell(new Phrase("TOTAL", smallfont));
            cellTOTAL.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellTOTAL.setBackgroundColor(BaseColor.LIGHT_GRAY);
            
            table.addCell(cellCANTIDAD);
            table.addCell(cellPRODUCTO);
            table.addCell(cellPRECIO);
            table.addCell(cellTOTAL);          

	        BigDecimal contador = new BigDecimal("0");
	        
	        for (int i = 0;i<venta.getDetalleVenta().size();i++) {
	        	
	        	
	            PdfPCell cell1 = new PdfPCell(new Phrase(String.valueOf(venta.getDetalleVenta().get(i).getCantidad()), smallfont));
	            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
	            
	            PdfPCell cell2 = new PdfPCell(new Phrase(venta.getDetalleVenta().get(i).getProducto().getNombre(), smallfont));
	            cell2.setHorizontalAlignment(Element.ALIGN_LEFT);

	            PdfPCell cell3 = new PdfPCell(new Phrase(venta.getDetalleVenta().get(i).getProducto().getPreciov(), smallfont));
	            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);

		        BigDecimal precio = new BigDecimal(venta.getDetalleVenta().get(i).getProducto().getPreciov());
				BigDecimal cantidad = new BigDecimal(venta.getDetalleVenta().get(i).getCantidad());
				BigDecimal subTotal = precio.multiply(cantidad);								
				contador = contador.add(subTotal);

	            PdfPCell cell4 = new PdfPCell(new Phrase(String.valueOf(subTotal), smallfont));
	            cell4.setHorizontalAlignment(Element.ALIGN_CENTER);

		        table.addCell(cell1);
		        table.addCell(cell2);
		        table.addCell(cell3);
		        table.addCell(cell4);
	        }
	        document.add(table);
	        
	        BigDecimal igv = new BigDecimal("0.18");
	        BigDecimal subTotal = contador;
	        BigDecimal Total = contador.add(contador.multiply(igv));
	        
    		Paragraph totalVenta = new Paragraph(8);
    		totalVenta.add(Chunk.NEWLINE);
    		if(venta.getTipodoc().equals("FACTURA")) {
    			totalVenta.add("SUB TOTAL: "+String.valueOf(subTotal));
    			totalVenta.add(Chunk.NEWLINE);
    			totalVenta.add(Chunk.NEWLINE);
        		totalVenta.add("IGV: "+String.valueOf("18%"));	
        		totalVenta.add(Chunk.NEWLINE);
        		totalVenta.add(Chunk.NEWLINE);
    		} 
    		DecimalFormat formatoDecimal = new DecimalFormat("####.##");    		
    		totalVenta.add("TOTAL: "+String.valueOf(formatoDecimal.format(Total)));
    		totalVenta.setFont(smallfont);
    		totalVenta.setAlignment(Element.ALIGN_RIGHT);
    		totalVenta.setFont(smallfont1);
    		
    		document.add(totalVenta);
		
	}
	
}
