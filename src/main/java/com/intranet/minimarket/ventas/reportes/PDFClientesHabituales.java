package com.intranet.minimarket.ventas.reportes;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.intranet.minimarket.ventas.entidades.entCliente;
import com.intranet.minimarket.ventas.util.util;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFClientesHabituales extends AbstractITextPdfView{

	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Font smallfont = new Font(FontFamily.HELVETICA, 10);	
		@SuppressWarnings("unchecked")
		ArrayList<entCliente> lista  = (ArrayList<entCliente>)model.get("listaClientesHabituales");
		
		Image image = Image.getInstance(PDFProductosBajoStock.class.getResource(util.getImagenes().getString("logo")));
		//image.scaleToFit(96f, 96f);
		Paragraph logo = new Paragraph(10);
		logo.add(image);
		logo.setFont(smallfont);
		logo.add("   MiniMarket");
		logo.add(Chunk.NEWLINE);
		logo.add(Chunk.NEWLINE);
		logo.add(Chunk.NEWLINE);
		document.add(logo);
		//c.codcliente, c.nombre, c.ape_pater, c.ape_mater, c.dni, c.direccion, c.telefono,
		PdfPTable table = new PdfPTable(5);
        table.setWidths(new float[] {10, 10, 10, 10, 10});
        table.setWidthPercentage(100);
		
        PdfPCell cellDNI = new PdfPCell(new Phrase("DNI", smallfont));
        cellDNI.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellDNI.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellNOMBRE = new PdfPCell(new Phrase("NOMBRE", smallfont));
        cellNOMBRE.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellNOMBRE.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellAPELLIDOS = new PdfPCell(new Phrase("APELLIDOS", smallfont));
        cellAPELLIDOS.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAPELLIDOS.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellDIRECION = new PdfPCell(new Phrase("DIRECION", smallfont));
        cellDIRECION.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellDIRECION.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        PdfPCell cellTELEFONO = new PdfPCell(new Phrase("TELEFONO", smallfont));
        cellTELEFONO.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellTELEFONO.setBackgroundColor(BaseColor.LIGHT_GRAY);
        
        table.addCell(cellDNI);
        table.addCell(cellNOMBRE);
        table.addCell(cellAPELLIDOS);
        table.addCell(cellDIRECION);  
        table.addCell(cellTELEFONO);  
        
        for(int i=0;i<lista.size();i++) {
            PdfPCell cell1 = new PdfPCell(new Phrase(lista.get(i).getDni(), smallfont));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            
            PdfPCell cell2 = new PdfPCell(new Phrase(lista.get(i).getNombre(), smallfont));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cell3 = new PdfPCell(new Phrase(lista.get(i).getApe_pater()+" "+lista.get(i).getApe_mater(), smallfont));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);

            PdfPCell cell4 = new PdfPCell(new Phrase(lista.get(i).getDireccion(), smallfont));
            cell4.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cell5 = new PdfPCell(new Phrase(lista.get(i).getTelefono(), smallfont));
            cell5.setHorizontalAlignment(Element.ALIGN_CENTER);

	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        table.addCell(cell4);
	        table.addCell(cell5);
        }
        document.add(table);		
	}	
}
