package com.intranet.minimarket.ventas.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.intranet.minimarket.ventas.entidades.entTrabajador;

public class daoTrabajador {
	// Singleton
		public static daoTrabajador _Instancia;
		private daoTrabajador() {};
		public static daoTrabajador Instancia() {
			if (_Instancia == null) {
				_Instancia = new daoTrabajador();
			}
			return _Instancia;
		}
		// endSingleton

		public entTrabajador VerificarAcceso(String _Usuario, String _Pass,boolean _x) throws Exception {
			Connection cn = null;
			entTrabajador t = null;
			CallableStatement cstm = null;
			ResultSet rs = null;
			try {

				cn = Conexion.Instancia().getConnection();
				cstm = cn.prepareCall("{call sp_verificarAcceso(?,?,?)}");
				cstm.setString(1, _Usuario);
				cstm.setString(2, _Pass);
				cstm.setBoolean(3, _x);
				rs = cstm.executeQuery();
				if (rs.next()) {

					t = new entTrabajador();
					
					t.setCodtrabajador(rs.getInt(1));
					t.setNombre(rs.getString(2));
					t.setApe_pater(rs.getString(3));
					t.setApe_mater(rs.getString(4));
					t.setDireccion(rs.getString(5));
					t.setTelefono(rs.getString(6));
					t.setUsuario(rs.getString(7));
					t.setPasswd(rs.getString(8));
					t.setTipo(rs.getBoolean(9));

				}
				return t;
			} catch (Exception e) {
				throw e;
			} finally {
				cn.close();
				cstm.close();
				rs.close();
			}
		}
		
		public ArrayList<entTrabajador> ListarTrabajadores() throws Exception {
			Connection cn = null;
			CallableStatement cstm=null;
			ResultSet rs = null;
			entTrabajador trabajador = null;
			ArrayList<entTrabajador> lista = new ArrayList<entTrabajador>();
			try {
				cn = Conexion.Instancia().getConnection();
				cstm = cn.prepareCall("{call sp_listarTrabajadores()}");
				rs = cstm.executeQuery();
				while(rs.next()) {
					trabajador = new entTrabajador();
					trabajador.setCodtrabajador(rs.getInt(1));
					trabajador.setNombre(rs.getString(2));
					trabajador.setApe_pater(rs.getString(3));
					trabajador.setApe_mater(rs.getString(4));
					trabajador.setDireccion(rs.getString(5));
					trabajador.setTelefono(rs.getString(6));
					trabajador.setUsuario(rs.getString(7));
					trabajador.setPasswd(rs.getString(8));
					trabajador.setTipo(rs.getBoolean(9));
					lista.add(trabajador);
				}
				return lista;
			} catch (Exception e) {
				throw e;
			} finally {
				cn.close();
				cstm.close();
				rs.close();
			}
		}
		
		public entTrabajador DevolverTrabajadorId(int id) throws Exception {
			Connection cn = null;
			entTrabajador trabajador = null;
			CallableStatement cstm = null;
			ResultSet rs = null;
			try {			
				cn = Conexion.Instancia().getConnection();
				cstm = cn.prepareCall("{call sp_DevolverTrabajadorId(?)}");
				cstm.setInt(1, id);
				rs = cstm.executeQuery();
				if (rs.next()) {					
					trabajador = new entTrabajador();
					trabajador.setCodtrabajador(rs.getInt(1));
					trabajador.setNombre(rs.getString(2));
					trabajador.setApe_pater(rs.getString(3));
					trabajador.setApe_mater(rs.getString(4));
					trabajador.setDireccion(rs.getString(5));
					trabajador.setTelefono(rs.getString(6));
					trabajador.setUsuario(rs.getString(7));
					trabajador.setPasswd(rs.getString(8));
					trabajador.setTipo(rs.getBoolean(9));											
				}
				return trabajador;
			} catch (Exception e) {
				throw e;
			} finally {
				cn.close();
				cstm.close();
				rs.close();
			}
		}

		public boolean nuevoTrabajador(entTrabajador trabajador) throws Exception {
			Connection cn = null;
			boolean nuevo = false;
			CallableStatement cstm = null;
			try {			
				cn = Conexion.Instancia().getConnection();
				cstm = cn.prepareCall("{call sp_nuevoTrabajador(?,?,?,?,?,?,?,?)}");
				cstm.setString(1, trabajador.getNombre());
				cstm.setString(2, trabajador.getApe_pater());
				cstm.setString(3, trabajador.getApe_mater());
				cstm.setString(4, trabajador.getDireccion());
				cstm.setString(5, trabajador.getTelefono());
				cstm.setString(6, trabajador.getUsuario());
				cstm.setString(7, trabajador.getPasswd());
				cstm.setBoolean(8, trabajador.isTipo());
				int x = cstm.executeUpdate();
				if(x!=0){
					nuevo = true;
				}
				
				return nuevo;
			} catch (Exception e) {
				throw e;
			} finally {
				cn.close();
				cstm.close();
			}
		}
		
		public boolean EditarTrabajador(entTrabajador trabajador) throws Exception {
			Connection cn = null;
			boolean editar = false;
			CallableStatement cstm = null;
			try {
				cn = Conexion.Instancia().getConnection();
				cstm = cn.prepareCall("{call sp_EditarTrabajador(?,?,?,?,?,?,?,?,?)}");
				cstm.setInt(1, trabajador.getCodtrabajador());
				cstm.setString(2, trabajador.getNombre());
				cstm.setString(3, trabajador.getApe_pater());
				cstm.setString(4, trabajador.getApe_mater());
				cstm.setString(5, trabajador.getDireccion());
				cstm.setString(6, trabajador.getTelefono());
				cstm.setString(7, trabajador.getUsuario());
				cstm.setString(8, trabajador.getPasswd());
				cstm.setBoolean(9, trabajador.isTipo());
				int x = cstm.executeUpdate();
				if(x!=0){
					editar = true;
				}
				
				return editar;
			} catch (Exception e) {
				throw e;
			} finally {
				cn.close();
				cstm.close();
			}
		}
		
		public boolean EliminarTrabajador(int id) throws Exception {
			Connection cn = null;
			boolean eliminar = false;
			CallableStatement cstm = null;
			try {
				cn = Conexion.Instancia().getConnection();
				cstm = cn.prepareCall("{call sp_EliminarTrabajador(?)}");
				cstm.setInt(1,id);
				int x = cstm.executeUpdate();
				if(x!=0){
					eliminar = true;
				}
				return eliminar;
			} catch (Exception e) {
				throw e;
			} finally {
				cn.close();
				cstm.close();
			}
		}
		
}
