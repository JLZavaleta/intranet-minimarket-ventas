package com.intranet.minimarket.ventas.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.intranet.minimarket.ventas.entidades.entCategoria;
import com.intranet.minimarket.ventas.entidades.entProducto;

public class daoProducto {
	// Singleton
		public static daoProducto _Instancia;
		private daoProducto() {};
		public static daoProducto Instancia() {
			if (_Instancia == null) {
				_Instancia = new daoProducto();
			}
			return _Instancia;
		}
		// endSingleton
		
		public ArrayList<entProducto> ListarProductos() throws Exception {
			Connection cn = null;
			CallableStatement cstm=null;
			ResultSet rs = null;
			entProducto pro = null;
			entCategoria cat = null;
			ArrayList<entProducto> lista = new ArrayList<entProducto>();
			try {
				cn = Conexion.Instancia().getConnection();
				cstm = cn.prepareCall("{call sp_listarProductos()}");
				rs = cstm.executeQuery();
				while(rs.next()) {
					pro = new entProducto();
					pro.setCodproducto(rs.getInt(1));
					
					cat = new entCategoria();
					cat.setIdtipo(rs.getInt(2));
															
					pro.setNombre(rs.getString(3));
					pro.setDescripcion(rs.getString(4));
					cat.setDescripcion(rs.getString(5));
					pro.setCategoria(cat);
					pro.setStock(String.valueOf(rs.getInt(6)));
					pro.setPreciov(rs.getString(7));
					lista.add(pro);

				}
				return lista;
			} catch (Exception e) {
				throw e;
			} finally {
				cn.close();
				cstm.close();
				rs.close();
			}
		}

		public entProducto DevolverProductoId(int id) throws Exception {
			Connection cn = null;
			entProducto pro = null;
			entCategoria cat = null;
			CallableStatement cstm = null;
			ResultSet rs = null;
			try {

				cn = Conexion.Instancia().getConnection();
				cstm = cn.prepareCall("{call sp_DevolverProductoId(?)}");
				cstm.setInt(1, id);
				rs = cstm.executeQuery();
				if (rs.next()) {					
					pro = new entProducto();
					pro.setCodproducto(rs.getInt(1));
					
					cat = new entCategoria();
					cat.setIdtipo(rs.getInt(2));
					pro.setCategoria(cat);
															
					pro.setNombre(rs.getString(3));
					pro.setDescripcion(rs.getString(4));					
					pro.setStock(String.valueOf(rs.getInt(5)));
					pro.setPreciov(rs.getString(6));
				}
				return pro;
			} catch (Exception e) {
				throw e;
			} finally {
				cn.close();
				cstm.close();
				rs.close();
			}
		}
		
		public boolean nuevoProducto(entProducto producto) throws Exception {
			Connection cn = null;
			boolean nuevo = false;
			CallableStatement cstm = null;
			try {

				cn = Conexion.Instancia().getConnection();
				cstm = cn.prepareCall("{call sp_nuevoProducto(?,?,?,?,?)}");
				cstm.setInt(1, producto.getCategoria().getIdtipo());
				cstm.setString(2, producto.getNombre());
				cstm.setString(3, producto.getDescripcion());
				cstm.setInt(4, Integer.parseInt(producto.getStock()));
				cstm.setString(5, producto.getPreciov());
				int x = cstm.executeUpdate();
				if(x!=0){
					nuevo = true;
				}
				
				return nuevo;
			} catch (Exception e) {
				throw e;
			} finally {
				cn.close();
				cstm.close();
			}
		}
		
		public boolean EditarProducto(entProducto producto) throws Exception {
			Connection cn = null;
			boolean editar = false;
			CallableStatement cstm = null;
			try {

				cn = Conexion.Instancia().getConnection();
				cstm = cn.prepareCall("{call sp_EditarProducto(?,?,?,?,?,?)}");
				cstm.setInt(1, producto.getCodproducto());
				cstm.setInt(2, producto.getCategoria().getIdtipo());
				cstm.setString(3, producto.getNombre());
				cstm.setString(4, producto.getDescripcion());
				cstm.setInt(5, Integer.parseInt(producto.getStock()));
				cstm.setString(6, producto.getPreciov());
				int x = cstm.executeUpdate();
				if(x!=0){
					editar = true;
				}
				
				return editar;
			} catch (Exception e) {
				throw e;
			} finally {
				cn.close();
				cstm.close();
			}
		}
		
		public boolean EliminarProducto(int id) throws Exception {
			Connection cn = null;
			boolean eliminar = false;
			CallableStatement cstm = null;
			try {
				cn = Conexion.Instancia().getConnection();
				cstm = cn.prepareCall("{call sp_EliminarProducto(?)}");
				cstm.setInt(1,id);
				int x = cstm.executeUpdate();
				if(x!=0){
					eliminar = true;
				}
				return eliminar;
			} catch (Exception e) {
				throw e;
			} finally {
				cn.close();
				cstm.close();
			}
		}
}
