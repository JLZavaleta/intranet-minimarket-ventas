package com.intranet.minimarket.ventas.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;

import com.intranet.minimarket.ventas.entidades.entCliente;
import com.intranet.minimarket.ventas.entidades.entDetalleVenta;
import com.intranet.minimarket.ventas.entidades.entProducto;
import com.intranet.minimarket.ventas.entidades.entVenta;

public class daoVenta {
	
	// Singleton
	public static daoVenta _Instancia;
	private daoVenta() {};
	public static daoVenta Instancia() {
		if (_Instancia == null) {
			_Instancia = new daoVenta();
		}
		return _Instancia;
	}
	// endSingleton
	
	private static Connection cn = null;
	public boolean nuevaVenta(entVenta venta)throws Exception{
		CallableStatement cstm = null;
		boolean x = false;
		try {			
			cn = Conexion.Instancia().getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall("{call sp_nuevaVenta(?,?,?,?,?)}");
			cstm.setInt(1, venta.getCliente().getCodcliente());
			cstm.setInt(2, venta.getTrabajador().getCodtrabajador());
			cstm.setString(3, venta.getTipodoc());
			cstm.setDate(4, venta.getF_emision());
			cstm.registerOutParameter(5, Types.INTEGER);
			int y = cstm.executeUpdate();
			if(y!=0){
				int id = cstm.getInt(5);
				if(DetalleVenta(id,venta.getDetalleVenta())){
					cn.commit();
					if(actualizarStock(venta.getDetalleVenta())){						
						x = true;
					}
				}else{
					cn.rollback();
				}
				
			}
			return x;
		} catch (Exception e) {
			cn.rollback();
			System.out.println(e.getMessage());
			throw e;
		}finally{
			cn.close();
			cstm.close();
		}
	}
	
	private boolean DetalleVenta(int id, ArrayList<entDetalleVenta> listaDetalle)throws Exception{
		CallableStatement cstm = null;
		boolean x = false;
		int y=0;
		try {			
			for(int i=0;i< listaDetalle.size();i++){
				cstm = cn.prepareCall("{call sp_nuevoDetalleVenta(?,?,?)}");
				cstm.setInt(1, id);
				cstm.setInt(2, listaDetalle.get(i).getProducto().getCodproducto());
				cstm.setInt(3, listaDetalle.get(i).getCantidad());
				y = cstm.executeUpdate();
			}
			
			if(y!=0){
				x = true;
			}
			return x;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}finally{
			cstm.close();
		}
	}
	
	private boolean actualizarStock(ArrayList<entDetalleVenta> listadetalle)throws Exception{
		Connection cn=null;
		CallableStatement cstm = null;
		boolean actualizar = false;
		try {
			cn = Conexion.Instancia().getConnection();
			for(int i = 0; i<listadetalle.size();i++){
				int stockActual = DevolverProductoId(listadetalle.get(i).getProducto().getCodproducto())-listadetalle.get(i).getCantidad();
				cstm = cn.prepareCall("{call sp_actualizarStock(?,?)}");
				cstm.setInt(1, listadetalle.get(i).getProducto().getCodproducto());
				cstm.setInt(2, stockActual);
				int y = cstm.executeUpdate();
				if(y!=0){
					actualizar = true;
				}
			}
			
		} catch (Exception e) {
			System.out.println("ERROR: "+e.getMessage());
			throw e;
		}finally {
			cstm.close();
			cn.close();
		}
		return actualizar;
	}
	
	private int DevolverProductoId(int id) throws Exception {
		Connection cn=null;
		entProducto pro = null;
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_stockPorIde(?)}");
			cstm.setInt(1, id);
			rs = cstm.executeQuery();
			if (rs.next()) {
				pro = new entProducto();
				pro.setStock(String.valueOf(rs.getInt(1)));
			}
			return Integer.parseInt(pro.getStock());
		} catch (Exception e) {
			throw e;
		} finally {
			cstm.close();
			rs.close();
			cn.close();
		}
	}
	
	public ArrayList<entVenta> ventasRealizadas() throws Exception {
		Connection cn = null;
		entCliente cli = null;
		entVenta venta = null;
		ArrayList<entVenta> lista = null;
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			/*
			 * v.numdocventa, v.codcliente, v.tipodoc, v.f_emision,c.nombre, c.ape_pater, c.ape_mater, c.dni
			 * */
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_ventasRealizadas()}");
			rs = cstm.executeQuery();
			lista = new ArrayList<entVenta>();
			while(rs.next()) {					
				venta = new entVenta();
				venta.setNumdocventa(rs.getInt(1));				
				
				cli = new entCliente();
				cli.setCodcliente(rs.getInt(2));
				
				venta.setTipodoc(rs.getString(3));
				venta.setF_emision(rs.getDate(4));
				
				cli.setNombre(rs.getString(5));
				cli.setApe_pater(rs.getString(6));
				cli.setApe_mater(rs.getString(7));
				cli.setDni(rs.getString(8));
				
				venta.setCliente(cli);
				lista.add(venta);
			}
			return lista;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
			rs.close();
		}
	}
	
	public entVenta ventaPdf(int id) throws Exception {

		entCliente cli = null;
		entVenta venta = null;
		entDetalleVenta detalle = null;
		entProducto producto = null;
		ArrayList<entDetalleVenta> listaDetalle = null;
		
		Connection cn = null;
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			/*
			 *  v.numdocventa, v.codcliente, v.tipodoc, v.f_emision,
				c.nombre, c.ape_pater, c.ape_mater, c.dni, c.direccion, c.telefono, 
				dv.codproducto, dv.cantidad,  
				p.nombre, p.descripcion
			 * */
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_ventaPdf(?)}");
			cstm.setInt(1, id);
			rs = cstm.executeQuery();
			
			venta = new entVenta();					
			cli = new entCliente();
				
			listaDetalle = new ArrayList<entDetalleVenta>();
				
			while(rs.next()) {		
				
				venta.setNumdocventa(rs.getInt(1));
				
				cli.setCodcliente(rs.getInt(2));
				
				venta.setTipodoc(rs.getString(3));
				venta.setF_emision(rs.getDate(4));
				
				cli.setNombre(rs.getString(5));
				cli.setApe_pater(rs.getString(6));
				cli.setApe_mater(rs.getString(7));
				cli.setDni(rs.getString(8));
				cli.setDireccion(rs.getString(9));
				cli.setTelefono(rs.getString(10));
				venta.setCliente(cli);
				
				detalle = new entDetalleVenta();
				
				producto = new entProducto();
				producto.setCodproducto(rs.getInt(11));
				
				detalle.setCantidad(rs.getInt(12));
				
				producto.setNombre(rs.getString(13));
				producto.setDescripcion(rs.getString(14));
				producto.setPreciov(rs.getString(15));
				detalle.setProducto(producto);
				listaDetalle.add(detalle);
			}
			
			venta.setDetalleVenta(listaDetalle);
			
			return venta;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
			rs.close();
		}
	}
	
}
