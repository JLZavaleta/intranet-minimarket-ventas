package com.intranet.minimarket.ventas.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.intranet.minimarket.ventas.entidades.entCategoria;

public class daoCategoria {
	
	// Singleton
	public static daoCategoria _Instancia;
	private daoCategoria() {};
	public static daoCategoria Instancia() {
		if (_Instancia == null) {
			_Instancia = new daoCategoria();
		}
		return _Instancia;
	}
	// endSingleton
	
	public ArrayList<entCategoria> ListarCategorias() throws Exception {
		Connection cn = null;
		CallableStatement cstm=null;
		ResultSet rs = null;
		entCategoria c = null;
		ArrayList<entCategoria> lista = new ArrayList<entCategoria>();
		try {
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_listarCategorias()}");
			rs = cstm.executeQuery();
			while(rs.next()) {
				c = new entCategoria();
				c.setIdtipo(rs.getInt(1));
				c.setDescripcion(rs.getString(2));
				lista.add(c);
			}
			return lista;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
			rs.close();
		}
	}
}
