package com.intranet.minimarket.ventas.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.intranet.minimarket.ventas.entidades.entCliente;

public class daoCliente {
	// Singleton
	public static daoCliente _Instancia;
	private daoCliente() {};
	public static daoCliente Instancia() {
		if (_Instancia == null) {
			_Instancia = new daoCliente();
		}
		return _Instancia;
	}
	// endSingleton
	
	public ArrayList<entCliente> ListarClientes() throws Exception {
		Connection cn = null;
		CallableStatement cstm=null;
		ResultSet rs = null;
		entCliente cli = null;
		ArrayList<entCliente> lista = new ArrayList<entCliente>();
		try {
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_listarClientes()}");
			rs = cstm.executeQuery();
			while(rs.next()) {
				cli = new entCliente();
				cli.setCodcliente(rs.getInt(1));
				cli.setNombre(rs.getString(2));
				cli.setApe_pater(rs.getString(3));
				cli.setApe_mater(rs.getString(4));
				cli.setDni(rs.getString(5));
				cli.setDireccion(rs.getString(6));
				cli.setTelefono(rs.getString(7));
				lista.add(cli);

			}
			return lista;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
			rs.close();
		}
	}
	
	public entCliente DevolverClienteId(int id) throws Exception {
		Connection cn = null;
		entCliente cli = null;
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {			
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_DevolverClienteId(?)}");
			cstm.setInt(1, id);
			rs = cstm.executeQuery();
			if (rs.next()) {					
				cli = new entCliente();
				cli.setCodcliente(rs.getInt(1));
				cli.setNombre(rs.getString(2));
				cli.setApe_pater(rs.getString(3));
				cli.setApe_mater(rs.getString(4));
				cli.setDni(rs.getString(5));
				cli.setDireccion(rs.getString(6));
				cli.setTelefono(rs.getString(7));												
			}
			return cli;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
			rs.close();
		}
	}
	
	public boolean DevolverClientePorDni(String id) {
		Connection cn = null;
		boolean dni = false;
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {			
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_DevolverClientePorDni(?)}");
			cstm.setString(1, id);
			rs = cstm.executeQuery();
			if (rs.next()) {		
				dni = true;										
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			try {
				cn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				cstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return dni;
	}
	
	public boolean nuevoCliente(entCliente cliente) throws Exception {
		Connection cn = null;
		boolean nuevo = false;
		CallableStatement cstm = null;
		try {			
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_nuevoCliente(?,?,?,?,?,?)}");
			cstm.setString(1, cliente.getNombre());
			cstm.setString(2, cliente.getApe_pater());
			cstm.setString(3, cliente.getApe_mater());
			cstm.setString(4, cliente.getDni());
			cstm.setString(5, cliente.getDireccion());
			cstm.setString(6, cliente.getTelefono());
			int x = cstm.executeUpdate();
			if(x!=0){
				nuevo = true;
			}
			
			return nuevo;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
		}
	}
	
	public boolean EditarCliente(entCliente cliente) throws Exception {
		Connection cn = null;
		boolean editar = false;
		CallableStatement cstm = null;
		try {
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_EditarCliente(?,?,?,?,?,?,?)}");
			cstm.setInt(1, cliente.getCodcliente());
			cstm.setString(2, cliente.getNombre());
			cstm.setString(3, cliente.getApe_pater());
			cstm.setString(4, cliente.getApe_mater());
			cstm.setString(5, cliente.getDni());
			cstm.setString(6, cliente.getDireccion());
			cstm.setString(7, cliente.getTelefono());
			int x = cstm.executeUpdate();
			if(x!=0){
				editar = true;
			}
			
			return editar;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
		}
	}
	
	public boolean EliminarCliente(int id) throws Exception {
		Connection cn = null;
		boolean eliminar = false;
		CallableStatement cstm = null;
		try {
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_EliminarCliente(?)}");
			cstm.setInt(1,id);
			int x = cstm.executeUpdate();
			if(x!=0){
				eliminar = true;
			}
			return eliminar;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
		}
	}
	
}
