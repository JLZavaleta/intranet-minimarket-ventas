package com.intranet.minimarket.ventas.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.intranet.minimarket.ventas.entidades.entCategoria;
import com.intranet.minimarket.ventas.entidades.entCliente;
import com.intranet.minimarket.ventas.entidades.entDetalleVenta;
import com.intranet.minimarket.ventas.entidades.entProducto;
import com.intranet.minimarket.ventas.entidades.entTrabajador;
import com.intranet.minimarket.ventas.entidades.entVenta;

public class daoReportes {
	
	// Singleton
	public static daoReportes _Instancia;
	private daoReportes() {};
	public static daoReportes Instancia() {
		if (_Instancia == null) {
			_Instancia = new daoReportes();
		}
		return _Instancia;
	}
	// endSingleton
	//reporte de Clientes INICIO
	public ArrayList<entCliente> ClientesHabituales() throws Exception {
		Connection cn = null;
		CallableStatement cstm=null;
		ResultSet rs = null;
		entCliente cli = null;
		ArrayList<entCliente> lista = new ArrayList<entCliente>();
		try {
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_clienteHabituales()}");
			rs = cstm.executeQuery();
			while(rs.next()) {
				cli = new entCliente();
				cli.setCodcliente(rs.getInt(1));
				cli.setNombre(rs.getString(2));
				cli.setApe_pater(rs.getString(3));
				cli.setApe_mater(rs.getString(4));
				cli.setDni(rs.getString(5));
				cli.setDireccion(rs.getString(6));
				cli.setTelefono(rs.getString(7));
				lista.add(cli);

			}
			return lista;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
			rs.close();
		}
	}
	//reporte de Clientes FIN
	
	//Reporte de productos INICIO
	public ArrayList<entProducto> productosConStockBajo() throws Exception {
		Connection cn = null;
		CallableStatement cstm=null;
		ResultSet rs = null;
		entProducto pro = null;
		entCategoria cat = null;
		ArrayList<entProducto> lista = new ArrayList<entProducto>();
		try {
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_productosConStockBajo()}");
			rs = cstm.executeQuery();
			while(rs.next()) {
				pro = new entProducto();
				pro.setCodproducto(rs.getInt(1));
				
				cat = new entCategoria();
				cat.setIdtipo(rs.getInt(2));
														
				pro.setNombre(rs.getString(3));
				pro.setDescripcion(rs.getString(4));
				cat.setDescripcion(rs.getString(5));
				pro.setCategoria(cat);
				pro.setStock(String.valueOf(rs.getInt(6)));
				pro.setPreciov(rs.getString(7));
				lista.add(pro);

			}
			return lista;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
			rs.close();
		}
	}
	
	public ArrayList<entProducto> productosMasVendidos() throws Exception {
		Connection cn = null;
		CallableStatement cstm=null;
		ResultSet rs = null;
		entProducto pro = null;
		entCategoria cat = null;
		ArrayList<entProducto> lista = new ArrayList<entProducto>();
		try {
			//p.codproducto, p.nombre, p.descripcion, tp.descripcion, p.stock, p.preciov
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_productosMasVendidos()}");
			rs = cstm.executeQuery();
			while(rs.next()) {
				pro = new entProducto();
				pro.setCodproducto(rs.getInt(1));														
				pro.setNombre(rs.getString(2));
				pro.setDescripcion(rs.getString(3));
				
				cat = new entCategoria();				
				cat.setDescripcion(rs.getString(4));
				
				pro.setCategoria(cat);
				pro.setStock(String.valueOf(rs.getInt(5)));
				pro.setPreciov(rs.getString(6));
				lista.add(pro);

			}
			return lista;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
			rs.close();
		}
	}
	
	public ArrayList<entProducto> productosMasVendidosLimite(int limite) throws Exception {
		Connection cn = null;
		CallableStatement cstm=null;
		ResultSet rs = null;
		entProducto pro = null;
		entCategoria cat = null;
		ArrayList<entProducto> lista = new ArrayList<entProducto>();
		try {
			//p.codproducto, p.nombre, p.descripcion, tp.descripcion, p.stock, p.preciov
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_productosMasVendidosLimite(?)}");
			cstm.setInt(1, limite);
			rs = cstm.executeQuery();
			while(rs.next()) {
				pro = new entProducto();
				pro.setCodproducto(rs.getInt(1));														
				pro.setNombre(rs.getString(2));
				pro.setDescripcion(rs.getString(3));
				
				cat = new entCategoria();				
				cat.setDescripcion(rs.getString(4));
				
				pro.setCategoria(cat);
				pro.setStock(String.valueOf(rs.getInt(5)));
				pro.setPreciov(rs.getString(6));
				lista.add(pro);

			}
			return lista;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
			rs.close();
		}
	}
	
	public ArrayList<entProducto> productosMasVendidosFechas(String fechaInicio, String fechaFin) throws Exception {
		Connection cn = null;
		CallableStatement cstm=null;
		ResultSet rs = null;
		entProducto pro = null;
		entCategoria cat = null;
		ArrayList<entProducto> lista = new ArrayList<entProducto>();
		try {
			//p.codproducto, p.nombre, p.descripcion, tp.descripcion, p.stock, p.preciov,
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_productosMasVendidosFechas(?,?)}");
			cstm.setDate(1, Date.valueOf(fechaInicio));
			cstm.setDate(2, Date.valueOf(fechaFin));
			rs = cstm.executeQuery();
			while(rs.next()) {
				pro = new entProducto();
				pro.setCodproducto(rs.getInt(1));														
				pro.setNombre(rs.getString(2));
				pro.setDescripcion(rs.getString(3));
				
				cat = new entCategoria();				
				cat.setDescripcion(rs.getString(4));
				
				pro.setCategoria(cat);
				pro.setStock(String.valueOf(rs.getInt(5)));
				pro.setPreciov(rs.getString(6));
				lista.add(pro);

			}
			return lista;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
			rs.close();
		}
	}
	
	public ArrayList<entProducto> productosMasVendidosFechasLimite(String fechaInicio, String fechaFin, int limite) throws Exception {
		Connection cn = null;
		CallableStatement cstm=null;
		ResultSet rs = null;
		entProducto pro = null;
		entCategoria cat = null;
		ArrayList<entProducto> lista = new ArrayList<entProducto>();
		try {
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_productosMasVendidosFechasLimite(?,?,?)}");
			cstm.setDate(1, Date.valueOf(fechaInicio));
			cstm.setDate(2, Date.valueOf(fechaFin));
			cstm.setInt(3, limite);
			rs = cstm.executeQuery();
			while(rs.next()) {
				pro = new entProducto();
				pro.setCodproducto(rs.getInt(1));														
				pro.setNombre(rs.getString(2));
				pro.setDescripcion(rs.getString(3));
				
				cat = new entCategoria();				
				cat.setDescripcion(rs.getString(4));
				
				pro.setCategoria(cat);
				pro.setStock(String.valueOf(rs.getInt(5)));
				pro.setPreciov(rs.getString(6));
				lista.add(pro);

			}
			return lista;
		} catch (Exception e) {
			throw e;
		} finally {			
			cstm.close();
			rs.close();
			cn.close();
		}
	}
	
	public ArrayList<entVenta> ventasRealizadasPorTrabajador() throws Exception {
		Connection cn = null;
		CallableStatement cstm=null;
		ResultSet rs = null;
		entTrabajador trabajador =null;
		entVenta venta = null;
		entCliente cliente = null;
		entDetalleVenta detalleVenta = null;
		entProducto producto = null;
		entCategoria categoria = null;
		entCategoria cat = null;
		ArrayList<entVenta> lista = new ArrayList<entVenta>();
		try {
			//trabajador t, venta v, cliente c, detalleventa dv, producto p, tipo_producto tp
			/*
			 *  t.nombre,t.ape_pater,t.ape_mater,t.direccion,t.telefono,t.usuario,t.passwd,t.tipo,c.codcliente,c.nombre,
c.ape_pater,c.ape_mater,c.dni,c.direccion,c.telefono,p.codproducto,p.idtipo,p.nombre,p.descripcion,
p.stock,p.preciov,
tp.descripcion,v.f_emision,count(p.codproducto) as cantidadProductosVendidos
			 * */
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_VentasRealizadasPorTrabajador()}");
			rs = cstm.executeQuery();
			while(rs.next()) {
				trabajador = new entTrabajador();
				trabajador.setNombre(rs.getString(1));
				trabajador.setApe_pater(rs.getString(2));
				trabajador.setApe_mater(rs.getString(3));
				trabajador.setDireccion(rs.getString(4));
				trabajador.setTelefono(rs.getString(5));
				trabajador.setUsuario(rs.getString(6));
				trabajador.setPasswd(rs.getString(7));
				trabajador.setTipo(rs.getBoolean(8));
				
				cliente = new entCliente();	
				cliente.setCodcliente(rs.getInt(9));
				cliente.setNombre(rs.getString(10));
				cliente.setApe_pater(rs.getString(11));
				cliente.setApe_mater(rs.getString(12));
				cliente.setDni(rs.getString(13));
				cliente.setDireccion(rs.getString(14));
				cliente.setTelefono(rs.getString(15));
				
				categoria = new entCategoria();
				producto = new entProducto();
				producto.setCodproducto(rs.getInt(16));
				categoria.setIdtipo(rs.getInt(17));
				producto.setNombre(rs.getString(18));
				producto.setDescripcion(rs.getString(19));
				producto.setStock(rs.getString(20));
				producto.setPreciov(rs.getString(21));
				categoria.setDescripcion(rs.getString(22));
				
				detalleVenta = new entDetalleVenta();
				detalleVenta.setCantidad(rs.getInt(24));
				detalleVenta.setProducto(producto);
				venta = new entVenta();
				venta.setF_emision(rs.getDate(23));
				venta.setTrabajador(trabajador);
				venta.setCliente(cliente);
				venta.setDt(detalleVenta);
				lista.add(venta);

			}
			return lista;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
			rs.close();
		}
	}
	
	public ArrayList<entTrabajador> trabajadoresConMasVentas() throws Exception {
		Connection cn = null;
		CallableStatement cstm=null;
		ResultSet rs = null;
		entTrabajador trabajador =null;
		ArrayList<entTrabajador> lista = new ArrayList<entTrabajador>();
		try {
			/*t.codtrabajador,
			 *  t.nombre, 
				t.ape_pater, 
				t.ape_mater, 
				t.direccion, 
				t.telefono, 
				t.usuario, 
				t.passwd, 
				t.tipo, count(p.codproducto) as totalVentas
			 * */
			cn = Conexion.Instancia().getConnection();
			cstm = cn.prepareCall("{call sp_trabajadoresConMasVentas()}");
			rs = cstm.executeQuery();
			while(rs.next()) {
				trabajador = new entTrabajador();
				trabajador.setCodtrabajador(rs.getInt(1));
				trabajador.setNombre(rs.getString(2));
				trabajador.setApe_pater(rs.getString(3));
				trabajador.setApe_mater(rs.getString(4));
				trabajador.setDireccion(rs.getString(5));
				trabajador.setTelefono(rs.getString(6));
				trabajador.setUsuario(rs.getString(7));
				trabajador.setPasswd(rs.getString(8));
				trabajador.setTipo(rs.getBoolean(9));
				trabajador.setCantidad(rs.getInt(10));
				lista.add(trabajador);
			}
			return lista;
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
			cstm.close();
			rs.close();
		}
	}
	
	//Reporte de productos FIN
}
