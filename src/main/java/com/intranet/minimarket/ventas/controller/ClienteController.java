package com.intranet.minimarket.ventas.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.intranet.minimarket.ventas.entidades.entCliente;
import com.intranet.minimarket.ventas.entidades.entTrabajador;
import com.intranet.minimarket.ventas.message.Response;
import com.intranet.minimarket.ventas.negocio.negCliente;

@Controller
@RequestMapping(value="/Cliente")
public class ClienteController {
	
	
	@RequestMapping(value="/lista",method=RequestMethod.GET)
	public ModelAndView listaCatProv(HttpServletRequest req){
		ModelAndView m=null;
		if(req.getSession().getAttribute("trabajador")!=null) {
			m = new ModelAndView("/Cliente/Lista");
			try {
				ArrayList<entCliente> lista = negCliente.Instancia().ListarClientes();
				m.addObject("listaClientes",lista);		
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}else {
			m = new ModelAndView("redirect:/");
		}
		return m;
	}	

	//#codcliente, nombre, ape_pater, ape_mater, direccion, telefono
	
	@RequestMapping(value = "/Nuevo", method = RequestMethod.GET)
	public ModelAndView NuevoProducto(HttpServletRequest req) { 
		if(req.getSession().getAttribute("trabajador")!=null) {
			return new ModelAndView("/Cliente/Nuevo","command",new entCliente());
		}else {
			return new ModelAndView("redirect:/");
		}		
	}
	
	@RequestMapping(value = "/GrabarNuevo", method = RequestMethod.POST)
	public ModelAndView GrabarNuevo(@ModelAttribute entCliente cliente, ModelMap model) {
		try {
			boolean inserto = negCliente.Instancia().nuevoCliente(cliente);
			 
			if(inserto) {
				return new ModelAndView("redirect:/Cliente/lista?msj=Se inserto satisfactoriamente");
			}else {
				return new ModelAndView("redirect:/Cliente/lista?msj=no se inserto ");
			}
		}catch(ArithmeticException ex){
			ModelAndView m = new ModelAndView("/Cliente/Nuevo","command",cliente);
			model.addAttribute("mensaje",ex.getMessage());		
			return m;
		}
		catch (Exception e) {
			return new ModelAndView("redirect:/frmError");	
		}

	}
	
	@RequestMapping(value = "/Editar", method = RequestMethod.GET)
	public ModelAndView EditarProducto(HttpServletRequest req) {
		if(req.getSession().getAttribute("trabajador")!=null) {
			try {			
				int id = Integer.parseInt(req.getParameter("id"));
				entCliente c = negCliente.Instancia().DevolverClienteId(id);
				ModelAndView m = new ModelAndView("/Cliente/Editar", "command", c);			
				return m;
			}
			catch (Exception e) {
				return new ModelAndView("redirect:/");	
			}
		}else {
			return new ModelAndView("login","command",new entTrabajador());
		}	

	}
	
	@RequestMapping(value = "/GrabarEditar", method = RequestMethod.POST)
	public ModelAndView GuardarEditar(@ModelAttribute entCliente cliente, ModelMap model) { 
						
		try {			
			boolean edito = negCliente.Instancia().EditarCliente(cliente);
			
			if(edito) {
				return new ModelAndView("redirect:/Cliente/lista?msj=Se Edito satisfactoriamente");
			}else{
				return new ModelAndView("redirect:/Cliente/lista?msj=No se Edito");
			}			
		}catch(ArithmeticException ex){
			ModelAndView m = new ModelAndView("/Cliente/Editar","command",cliente);
			model.addAttribute("mensaje",ex.getMessage());		
			return m;
		}
		catch (Exception e) {
			return new ModelAndView("redirect:/frmError");	
		}

	}
	
	@RequestMapping(value = "/GuardarEliminar", method = RequestMethod.POST)
	public ModelAndView GuardarEliminarProducto(@ModelAttribute entCliente cliente) { 
		try {
			boolean eliminar = negCliente.Instancia().EliminarCliente(cliente.getCodcliente());
			if(eliminar) {
				return new ModelAndView("redirect:/Cliente/lista?msj=Se Elimino satisfactoriamente");
			}else {
				return new ModelAndView("redirect:/Cliente/lista?msj=No se Elimino");
			}
			
		} catch (Exception e) {
			return new ModelAndView("redirect:/frmError");	
		}

	}
	
	@RequestMapping(value = "/Eliminar", method = RequestMethod.GET)
	public ModelAndView EliminarProducto(HttpServletRequest req) {
		if(req.getSession().getAttribute("trabajador")!=null) {
			try {
				int id = Integer.parseInt(req.getParameter("id"));
				entCliente c = negCliente.Instancia().DevolverClienteId(id);
				ModelAndView m =  new ModelAndView("/Cliente/Eliminar", "command",c);					
				return m;
			} catch (Exception e) {
				return new ModelAndView("redirect:/frmError");	
			}
		}else {
			return new ModelAndView("redirect:/");
		}	
	}
	//================================REST==========================================//
	@RequestMapping(value ="/listar", method = RequestMethod.GET,produces="application/json")
	public @ResponseBody Response Lista() {		
		ArrayList<entCliente> lista=null;
		Response response = null;
		try {
			lista = negCliente.Instancia().ListarClientes();
			response = new Response("Done",lista);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	//================================REST==========================================//
}
