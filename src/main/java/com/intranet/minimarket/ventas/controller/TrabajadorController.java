package com.intranet.minimarket.ventas.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.intranet.minimarket.ventas.entidades.entTrabajador;
import com.intranet.minimarket.ventas.entidades.entVenta;
import com.intranet.minimarket.ventas.negocio.negReportes;
import com.intranet.minimarket.ventas.negocio.negTrabajador;

@Controller
@RequestMapping(value="/Trabajador")
public class TrabajadorController {
	@RequestMapping(value="/lista",method=RequestMethod.GET)
	public ModelAndView listaTrabajadores(HttpServletRequest req){
		ModelAndView m=null;
		if(req.getSession().getAttribute("trabajador")!=null) {
			entTrabajador t = (entTrabajador)req.getSession().getAttribute("trabajador");
			if(t.isTipo()!=false) {
			m = new ModelAndView("/Trabajador/Lista");
			try {
				ArrayList<entTrabajador> lista = negTrabajador.Instancia().ListarTrabajadores();
				m.addObject("listaTrabajadores",lista);		
			} catch (Exception e) {
				e.printStackTrace();
			}
			}else {
				m = new ModelAndView("redirect:/");
			}
		}else {
			m = new ModelAndView("redirect:/");
		}
		return m;
	}	

	
	@RequestMapping(value = "/Nuevo", method = RequestMethod.GET)
	public ModelAndView NuevoTrabajador(HttpServletRequest req) { 
		if(req.getSession().getAttribute("trabajador")!=null) {
			entTrabajador t = (entTrabajador)req.getSession().getAttribute("trabajador");
			if(t.isTipo()!=false) {
			return new ModelAndView("/Trabajador/Nuevo","command",new entTrabajador());
			}else {
				return new ModelAndView("redirect:/");
			}
		}else {
			return new ModelAndView("redirect:/");
		}		
	}
	
	@RequestMapping(value = "/GrabarNuevo", method = RequestMethod.POST)
	public ModelAndView GrabarNuevo(@ModelAttribute entTrabajador trabajador, ModelMap model) {
		try {
			boolean inserto = negTrabajador.Instancia().nuevoTrabajador(trabajador);			 
			if(inserto) {
				return new ModelAndView("redirect:/Trabajador/lista?msj=Se inserto satisfactoriamente");
			}else {
				return new ModelAndView("redirect:/Trabajador/lista?msj=no se inserto ");
			}
		}catch(ArithmeticException ex){
			ModelAndView m = new ModelAndView("/Trabajador/Nuevo","command",trabajador);
			model.addAttribute("mensaje",ex.getMessage());		
			return m;
		}
		catch (Exception e) {
			return new ModelAndView("redirect:/frmError");	
		}

	}
	
	@RequestMapping(value = "/Editar", method = RequestMethod.GET)
	public ModelAndView EditarTrabajador(HttpServletRequest req) {
		if(req.getSession().getAttribute("trabajador")!=null) {
			entTrabajador t = (entTrabajador)req.getSession().getAttribute("trabajador");
			if(t.isTipo()!=false) {
			try {			
				int id = Integer.parseInt(req.getParameter("id"));
				entTrabajador trab = negTrabajador.Instancia().DevolverTrabajadorId(id);
				ModelAndView m = new ModelAndView("/Trabajador/Editar", "command", trab);			
				return m;
			}
			catch (Exception e) {
				return new ModelAndView("redirect:/");	
			}
			}else {
				return new ModelAndView("login","command",new entTrabajador());
			}
		}else {
			return new ModelAndView("login","command",new entTrabajador());
		}
	}
	
	@RequestMapping(value = "/GrabarEditar", method = RequestMethod.POST)
	public ModelAndView GuardarEditar(@ModelAttribute entTrabajador trabajador, ModelMap model) { 
						
		try {			
			boolean edito = negTrabajador.Instancia().EditarTrabajador(trabajador);
			
			if(edito) {
				return new ModelAndView("redirect:/Trabajador/lista?msj=Se Edito satisfactoriamente");
			}else{
				return new ModelAndView("redirect:/Trabajador/lista?msj=No se Edito");
			}			
		}catch(ArithmeticException ex){
			ModelAndView m = new ModelAndView("/Trabajador/Editar","command",trabajador);
			model.addAttribute("mensaje",ex.getMessage());		
			return m;
		}
		catch (Exception e) {
			return new ModelAndView("redirect:/frmError");	
		}

	}
	
	@RequestMapping(value = "/GuardarEliminar", method = RequestMethod.POST)
	public ModelAndView GuardarEliminarProducto(@ModelAttribute entTrabajador trabajador) { 
		try {
			boolean eliminar = negTrabajador.Instancia().EliminarTrabajador(trabajador.getCodtrabajador());
			if(eliminar) {
				return new ModelAndView("redirect:/Trabajador/lista?msj=Se Elimino satisfactoriamente");
			}else {
				return new ModelAndView("redirect:/Trabajador/lista?msj=No se Elimino");
			}
			
		} catch (Exception e) {
			return new ModelAndView("redirect:/frmError");	
		}

	}
	
	@RequestMapping(value = "/Eliminar", method = RequestMethod.GET)
	public ModelAndView EliminarProducto(HttpServletRequest req) {
		if(req.getSession().getAttribute("trabajador")!=null) {
			entTrabajador tr = (entTrabajador)req.getSession().getAttribute("trabajador");
			if(tr.isTipo()!=false) {
			try {
				int id = Integer.parseInt(req.getParameter("id"));
				entTrabajador t = negTrabajador.Instancia().DevolverTrabajadorId(id);
				ModelAndView m =  new ModelAndView("/Trabajador/Eliminar", "command",t);					
				return m;
			} catch (Exception e) {
				return new ModelAndView("redirect:/frmError");	
			}
			}else {
				return new ModelAndView("redirect:/");
			}	
		}else {
			return new ModelAndView("redirect:/");
		}	
	}

	@RequestMapping(value="/lista/ventasRealizada/trabajador",method=RequestMethod.GET)
	public ModelAndView listaVentasRealizadasPorTrabajador(HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			ModelAndView m = null;
			entTrabajador t = (entTrabajador)req.getSession().getAttribute("trabajador");
			if(t.isTipo()!=false) {
			try {
				ArrayList<entVenta> lista = negReportes.Instancia().ventasRealizadasPorTrabajador();				
				if(lista!=null) {
					m = new ModelAndView("/Reportes/listaVentasRealizadasPorTrabajador");
					m.addObject("listaventasRealizadasPorTrabajador",lista);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}		
			return m;
			}else {
				return new ModelAndView("redirect:/");
			}
		}else {
			return new ModelAndView("redirect:/");
		}				
	}
	
	@RequestMapping(value="/lista/trabajador/ventas",method=RequestMethod.GET)
	public ModelAndView listaTrabajadoresConMasVentas(HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			ModelAndView m = null;
			entTrabajador t = (entTrabajador)req.getSession().getAttribute("trabajador");
			if(t.isTipo()!=false) {
			try {
				ArrayList<entTrabajador> lista = negReportes.Instancia().trabajadoresConMasVentas();				
				if(lista!=null) {
					m = new ModelAndView("/Reportes/listaTrabajadoresConMasVentas");
					m.addObject("listatrabajadoresConMasVentas",lista);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}		
			return m;
			}else {
				return new ModelAndView("redirect:/");
			}		
		}else {
			return new ModelAndView("redirect:/");
		}				
	}
	
}
