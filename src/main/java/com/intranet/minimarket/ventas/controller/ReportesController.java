package com.intranet.minimarket.ventas.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.intranet.minimarket.ventas.entidades.entCliente;
import com.intranet.minimarket.ventas.entidades.entObjPV;
import com.intranet.minimarket.ventas.entidades.entProducto;
import com.intranet.minimarket.ventas.entidades.entTrabajador;
import com.intranet.minimarket.ventas.entidades.entVenta;
import com.intranet.minimarket.ventas.message.Response;
import com.intranet.minimarket.ventas.negocio.negReportes;

@Controller
@RequestMapping(value="/Reportes")
public class ReportesController {
	
	@RequestMapping(value="/clientes/habituales",method=RequestMethod.GET)
	public ModelAndView clientesHabituales(HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			ModelAndView m = null;
			entTrabajador tr = (entTrabajador)req.getSession().getAttribute("trabajador");
			if(tr.isTipo()!=false) {
			m = new ModelAndView("/Reportes/ClientesHabituales");
			try {
				ArrayList<entCliente> lista = negReportes.Instancia().ClientesHabituales();
				m.addObject("listaClientesHabituales",lista);		
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return m;
			}else {
				return new ModelAndView("redirect:/");
			}	
		}else {
			return new ModelAndView("redirect:/");
		}				
	}
	@RequestMapping(value="/productos/stockBajo")
	public ModelAndView productosConStockBajo(HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			ModelAndView m = new ModelAndView("/Reportes/productosConBajoStock");
			try {
				ArrayList<entProducto> lista = negReportes.Instancia().productosConStockBajo();
				m.addObject("listaProductosConBajoStock",lista);
			
			} catch (Exception e) {
				e.printStackTrace();
			}
			return m;
		}else {
			return new ModelAndView("redirect:/");
		}				
	}
	
	@RequestMapping(value="/pdf/ventas")
	public ModelAndView productosVentas(HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			ModelAndView m = new ModelAndView("/Reportes/ProductosVendidos","command",new entObjPV());
			try {
				ArrayList<entProducto> lista = negReportes.Instancia().productosMasVendidos();
				m.addObject("lista",lista);
			
			} catch (Exception e) {
				e.printStackTrace();
			}
			return m;
		}else {
			return new ModelAndView("redirect:/");
		}				
	}
	
	//PDFS	
	@RequestMapping(value="/pdf/clientes/habituales",method=RequestMethod.GET)
	public ModelAndView PdfClientesHbituales(HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			ModelAndView m = null;
			entTrabajador tr = (entTrabajador)req.getSession().getAttribute("trabajador");
			if(tr.isTipo()!=false) {
			try {
				ArrayList<entCliente> lista = negReportes.Instancia().ClientesHabituales();
					
				if(lista!=null) {
					m = new ModelAndView("pdfClientesHabituales");
					m.addObject("listaClientesHabituales",lista);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}		
			}else {
				return new ModelAndView("redirect:/");
			}	
			return m;
		}else {
			return new ModelAndView("redirect:/");
		}				
	}
	
	@RequestMapping(value="/pdf/productos/bajoStock",method=RequestMethod.GET)
	public ModelAndView PdfProductosConBajoStock(HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			ModelAndView m = null;
			try {
				ArrayList<entProducto> lista = negReportes.Instancia().productosConStockBajo();				
				if(lista!=null) {
					m = new ModelAndView("pdfProductosConBajoStock");
					m.addObject("listaProductosConBajoStock",lista);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}		
			return m;
		}else {
			return new ModelAndView("redirect:/");
		}				
	}
	
	@RequestMapping(value="/pdf/ventasRealizada/trabajado",method=RequestMethod.GET)
	public ModelAndView PdfventasRealizadasPorTrabajador(HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			ModelAndView m = null;
			entTrabajador tr = (entTrabajador)req.getSession().getAttribute("trabajador");
			if(tr.isTipo()!=false) {
			try {
				ArrayList<entVenta> lista = negReportes.Instancia().ventasRealizadasPorTrabajador();				
				if(lista!=null) {
					m = new ModelAndView("pdfventasRealizadasPorTrabajador");
					m.addObject("listaventasRealizadasPorTrabajador",lista);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}		
			}else {
				return new ModelAndView("redirect:/");
			}	
			return m;
		}else {
			return new ModelAndView("redirect:/");
		}				
	}
	
	@RequestMapping(value="/pdf/trabajador/ventas",method=RequestMethod.GET)
	public ModelAndView PdftrabajadoresConMasVentas(HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			ModelAndView m = null;
			entTrabajador tr = (entTrabajador)req.getSession().getAttribute("trabajador");
			if(tr.isTipo()!=false) {			
			try {
				ArrayList<entTrabajador> lista = negReportes.Instancia().trabajadoresConMasVentas();				
				if(lista!=null) {
					m = new ModelAndView("pdftrabajadoresConMasVentas");
					m.addObject("listatrabajadoresConMasVentas",lista);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			}else {
				return new ModelAndView("redirect:/");
			}
			return m;
		}else {
			return new ModelAndView("redirect:/");
		}				
	}
	
	//Producto venta
	//================================REST==========================================//
	@RequestMapping(value="/pdf/productosVenta",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody Response PdfProductosVenta(@RequestBody entObjPV obj, HttpServletRequest req){
		Response response = null;
		if(req.getSession().getAttribute("trabajador")!=null) {			
			try {
				int valor=0;
				int limite=0;
				String fechaInicio = null;
				String fechaFin = null;
				
				fechaInicio=obj.getFechaInicio();
				fechaFin=obj.getFechaFin();
				limite= obj.getLimite();
				
				ArrayList<entProducto> lista = null;	
				
				if(limite==0&&fechaFin==null&&fechaFin==null) {
					valor=1;
				}
				if(limite!=0&&fechaFin==null&&fechaFin==null) {
					valor=2;
				}
				if(limite==0&&fechaFin!=null&&fechaFin!=null) {
					valor=3;
				}
				if(limite!=0&&fechaFin!=null&&fechaFin!=null) {
					valor=4;
				}
				switch (valor) {
				case 1:
					lista = negReportes.Instancia().productosMasVendidos();
					break;
				case 2:
					lista = negReportes.Instancia().productosMasVendidosLimite(limite);
					break;
				case 3:
					lista = negReportes.Instancia().productosMasVendidosFechas(fechaInicio, fechaFin);
					break;
				case 4:
					lista = negReportes.Instancia().productosMasVendidosFechasLimite(fechaInicio, fechaFin, limite);					
					break;
				default:
					break;
				}							
				if(lista!=null) {	
					response = new Response("Done",lista);									
				}else {
					response = new Response("Fail","Error");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}		
			return response;
		}else {
			return response;
		}				
	}
	
	
	@RequestMapping(value="/Productos/mas/vendidos",method=RequestMethod.GET)
	public @ResponseBody Response listarest(HttpServletRequest req){
		Response response=null;		
			if(req.getSession().getAttribute("trabajador")!=null) {
				ModelAndView m = null;
				try {
					int valor=0;
					int limite=0;
					String fechaInicio = null;
					String fechaFin = null;
					ArrayList<entProducto> lista = null;						
					switch (valor) {
					case 1:
						lista = negReportes.Instancia().productosMasVendidos();
						break;
					case 2:
						lista = negReportes.Instancia().productosMasVendidosLimite(limite);
						break;
					case 3:
						lista = negReportes.Instancia().productosMasVendidosFechas(fechaInicio, fechaFin);
						break;
					case 4:
						lista = negReportes.Instancia().productosMasVendidosFechasLimite(fechaInicio, fechaFin, limite);					
						break;
					default:
						break;
					}							
					if(lista!=null) {	
						m = new ModelAndView("pdfProductosVenta");
						m.addObject("listaProductosVenta",lista);
						m.addObject("valor", valor);
						m.addObject("limite",limite);
						m.addObject("fechaInicio", fechaInicio);
						m.addObject("fechaFin",fechaFin);
						response = new Response("Done",lista);
						
					}else {
						m = new ModelAndView("frmError");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}		
				return response;
			}else {
				return response;
			}				
	}	
		
	//================================REST==========================================//
	
}
