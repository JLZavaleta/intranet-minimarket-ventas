package com.intranet.minimarket.ventas.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MenuController {
	
	@RequestMapping(value = "/Menu", method = RequestMethod.GET)
	public ModelAndView home(Locale locale, Model model, HttpServletRequest req) {
		if(req.getSession().getAttribute("trabajador")!=null) {
			return new ModelAndView("menu");
		}else {
			return new ModelAndView("redirect:/");
		}
		
	}
}
