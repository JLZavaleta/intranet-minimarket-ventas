package com.intranet.minimarket.ventas.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.intranet.minimarket.ventas.entidades.entTrabajador;
import com.intranet.minimarket.ventas.negocio.negTrabajador;

/**
 * Handles requests for the application home page.
 */
@Controller
public class LoginController {
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home(Locale locale, Model model) {

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return new ModelAndView("login","command",new entTrabajador());
	}
	
	@RequestMapping(value = "/VerificarAcceso", method = RequestMethod.POST)
	public ModelAndView verificarAcceso(@ModelAttribute("SpringWeb")entTrabajador t, ModelMap model, HttpServletRequest req){
		try {
			entTrabajador ux=null;
			String _Usuario = t.getUsuario();
			String _password = t.getPasswd();
			boolean _x = t.isTipo();
			ux = negTrabajador.Instancia().verificarAcceso(_Usuario, _password,_x);
			req.getSession().setAttribute("trabajador", ux);
			if(req.getSession().getAttribute("trabajador")!=null) {
				return new ModelAndView("menu");
			}else{
				return new ModelAndView("login","command",new entTrabajador());
			} 
		} catch(ArithmeticException ex){
			model.addAttribute("mensaje",ex.getMessage());
			//model.addAttribute("command",t);
			return new ModelAndView("login","command",new entTrabajador());
		}catch (Exception ex) {
			model.addAttribute("mensaje",ex.getMessage());
			return new ModelAndView("frmError");
		}
		
	}
	/*
	@ModelAttribute("favouriteList")
	public List getFavouriteSports()
	{
		return "Admin";
	}*/
	
	@RequestMapping(value = "/CerrarSesion", method = RequestMethod.GET)
	public ModelAndView CerrarSesion(Locale locale, Model model, HttpServletRequest req) {
		req.getSession().removeAttribute("trabajador");
		return new ModelAndView("login", "command", new entTrabajador());
		
	}
	
}
