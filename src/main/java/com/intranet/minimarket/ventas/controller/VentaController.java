package com.intranet.minimarket.ventas.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.intranet.minimarket.ventas.entidades.entTrabajador;
import com.intranet.minimarket.ventas.entidades.entVenta;
import com.intranet.minimarket.ventas.message.Response;
import com.intranet.minimarket.ventas.negocio.negVenta;

@Controller
@RequestMapping(value="/Venta")
public class VentaController {

	@RequestMapping(value="/venta",method=RequestMethod.GET)
	public ModelAndView listaCatProv(HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			return new ModelAndView("/Venta/Venta");
		}else {
			return new ModelAndView("redirect:/");
		}				
	}
	
	@RequestMapping(value="/ventas/realizadas",method=RequestMethod.GET)
	public ModelAndView ventasRealizadas(HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			ModelAndView m = new ModelAndView("/Venta/VentasRealizadas");
			try {
				ArrayList<entVenta> ventas = negVenta.Instancia().ventasRealizadas();
				m.addObject("ventasRealizadas",ventas);
			} catch (Exception e) {
				e.printStackTrace();
			}		
			return m;
		}else {
			return new ModelAndView("redirect:/");
		}				
	}
	
	@RequestMapping(value="/ventas/pdf",method=RequestMethod.GET)
	public ModelAndView ventapdf(HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			ModelAndView m = new ModelAndView("/Venta/VentasRealizadas");
			entVenta venta = null;
			try {
				int id = Integer.parseInt(req.getParameter("id"));
				venta = negVenta.Instancia().ventaPdf(id);
				if(venta!=null) {
					m = new ModelAndView("pdfView", "ventaMasDetalle", venta);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}		
			return m;
		}else {
			return new ModelAndView("redirect:/");
		}				
	}
	
	//================================REST==========================================//

	//==================================NUEVA=VENTA=========================================//
	@RequestMapping(value="/nuevaVenta",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody Response nuevaVenta(@RequestBody entVenta venta, HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			entTrabajador trabajador = (entTrabajador)req.getSession().getAttribute("trabajador");
			venta.setTrabajador(trabajador);
		}		
		Response response=null;
		
		try {
			boolean x = negVenta.Instancia().nuevaVenta(venta);
			if(x) {
				response = new Response("Done","ingreso");				
			}else {
				response = new Response("Fail","Error");
			}			
		
		}catch(ArithmeticException ex){
			response = new Response("Validacion",ex.getMessage());		
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return response;
	}	
	
	//================================REST==========================================//
	
}
