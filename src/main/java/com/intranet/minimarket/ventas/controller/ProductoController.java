package com.intranet.minimarket.ventas.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.intranet.minimarket.ventas.entidades.entCategoria;
import com.intranet.minimarket.ventas.entidades.entProducto;
import com.intranet.minimarket.ventas.message.Response;
import com.intranet.minimarket.ventas.negocio.negCategoria;
import com.intranet.minimarket.ventas.negocio.negProducto;

@Controller
public class ProductoController {
	
	@RequestMapping(value = "/Producto/Nuevo", method = RequestMethod.GET)
	public ModelAndView NuevoProducto(HttpServletRequest req) { 
		if(req.getSession().getAttribute("trabajador")!=null) {
			ModelAndView m = new ModelAndView("/Producto/Nuevo","command",new entProducto());
			try {
				ArrayList<entCategoria> lista = negCategoria.Instancia().ListarCategorias();
				m.addObject("listaCategorias",lista);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return m;
		}else {
			return new ModelAndView("redirect:/");
		}		
	}
	
	@RequestMapping(value="/Producto/lista",method=RequestMethod.GET)
	public ModelAndView listaCatProv(HttpServletRequest req){
		if(req.getSession().getAttribute("trabajador")!=null) {
			ModelAndView m = new ModelAndView("/Producto/Lista");
			try {
				ArrayList<entProducto> lista = negProducto.Instancia().ListarProductos();
				m.addObject("listaProductos",lista);
			
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return m;
		}else {
			return new ModelAndView("redirect:/");
		}	
	}

	@RequestMapping(value = "/Producto/GrabarNuevo", method = RequestMethod.POST)
	public ModelAndView GrabarNuevo(@ModelAttribute entProducto producto, ModelMap model) {
		
		ArrayList<entCategoria> lista=null;
		try {
			 lista = negCategoria.Instancia().ListarCategorias();
			 boolean inserto = negProducto.Instancia().nuevoProducto(producto);
			 
			if(inserto) {
				return new ModelAndView("redirect:/Producto/lista?msj=Se inserto satisfactoriamente");
			}else {
				return new ModelAndView("redirect:/Producto/lista?msj=no se inserto ");
			}
		}catch(ArithmeticException ex){
			ModelAndView m = new ModelAndView("/Producto/Nuevo","command",producto);
			model.addAttribute("mensaje",ex.getMessage());		
			m.addObject("listaCategorias",lista);
			return m;
		}
		catch (Exception e) {
			return new ModelAndView("redirect:/frmError");	
		}

	}
	
	@RequestMapping(value = "/Producto/Editar", method = RequestMethod.GET)
	public ModelAndView EditarProducto(HttpServletRequest req) {
		if(req.getSession().getAttribute("trabajador")!=null) {
			try {				
				int id = Integer.parseInt(req.getParameter("id"));
				entProducto c = negProducto.Instancia().DevolverProductoId(id);
				ModelAndView m = new ModelAndView("/Producto/Editar", "command", c);
				ArrayList<entCategoria> lista = negCategoria.Instancia().ListarCategorias();
				m.addObject("listaCategorias",lista);			
				return m;
			
			}
			catch (Exception e) {
				return new ModelAndView("redirect:/frmError");	
			}
		}else {
			return new ModelAndView("redirect:/");
		}
	}
	
	@RequestMapping(value = "/Producto/GrabarEditar", method = RequestMethod.POST)
	public ModelAndView GuardarEditar(@ModelAttribute entProducto producto, ModelMap model) { 
				
		ArrayList<entCategoria> lista=null;
		
		try {		
			lista = negCategoria.Instancia().ListarCategorias();
			boolean edito = negProducto.Instancia().EditarProducto(producto);
			
			if(edito) {
				return new ModelAndView("redirect:/Producto/lista?msj=Se Edito satisfactoriamente");
			}else{
				return new ModelAndView("redirect:/Producto/lista?msj=No se Edito");
			}			
		}catch(ArithmeticException ex){
			ModelAndView m = new ModelAndView("/Producto/Editar","command",producto);
			model.addAttribute("mensaje",ex.getMessage());		
			m.addObject("listaCategorias",lista);
			return m;
		}
		catch (Exception e) {
			return new ModelAndView("redirect:/frmError");	
		}

	}
	
	@RequestMapping(value = "/Producto/GuardarEliminar", method = RequestMethod.POST)
	public ModelAndView GuardarEliminarProducto(@ModelAttribute entProducto producto) { 
		try {
			boolean eliminar = negProducto.Instancia().EliminarProducto(producto.getCodproducto());
			if(eliminar) {
				return new ModelAndView("redirect:/Producto/lista?msj=Se Elimino satisfactoriamente");
			}else {
				return new ModelAndView("redirect:/Producto/lista?msj=No se Elimino");
			}
			
		} catch (Exception e) {
			return new ModelAndView("redirect:/frmError");	
		}

	}
	
	@RequestMapping(value = "/Producto/Eliminar", method = RequestMethod.GET)
	public ModelAndView EliminarProducto(HttpServletRequest req) {
		if(req.getSession().getAttribute("trabajador")!=null) {
			try {
				int id = Integer.parseInt(req.getParameter("id"));
				entProducto u = negProducto.Instancia().DevolverProductoId(id);
				ModelAndView m =  new ModelAndView("/Producto/Eliminar", "command", u);
				
				ArrayList<entCategoria> lista = negCategoria.Instancia().ListarCategorias();
				m.addObject("listaCategorias",lista);
				
				return m;
			} catch (Exception e) {
				return new ModelAndView("redirect:/frmError");	
			}
		}else {
			return new ModelAndView("redirect:/");
		}	
	}
	//================================REST==========================================//
	@RequestMapping(value="/Producto/listarest",method=RequestMethod.GET)
	public @ResponseBody Response listarest(){
		Response response=null;
		
		try {
			ArrayList<entProducto> lista = negProducto.Instancia().ListarProductos();
			response = new Response("Done",lista);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}	
		
	//================================REST==========================================//
}