package com.intranet.minimarket.ventas.entidades;

import java.sql.Date;
import java.util.ArrayList;

public class entVenta {
	
	private int numdocventa;
	private entCliente cliente;
	private entTrabajador trabajador;
	private String tipodoc;
	private Date f_emision;
	private ArrayList<entDetalleVenta> detalleVenta;
	private entDetalleVenta dt;
	
	public int getNumdocventa() {
		return numdocventa;
	}
	public void setNumdocventa(int numdocventa) {
		this.numdocventa = numdocventa;
	}
	public entCliente getCliente() {
		return cliente;
	}
	public void setCliente(entCliente cliente) {
		this.cliente = cliente;
	}
	public entTrabajador getTrabajador() {
		return trabajador;
	}
	public void setTrabajador(entTrabajador trabajador) {
		this.trabajador = trabajador;
	}
	public String getTipodoc() {
		return tipodoc;
	}
	public void setTipodoc(String tipodoc) {
		this.tipodoc = tipodoc;
	}
	public Date getF_emision() {
		return f_emision;
	}
	public void setF_emision(Date f_emision) {
		this.f_emision = f_emision;
	}
	public ArrayList<entDetalleVenta> getDetalleVenta() {
		return detalleVenta;
	}
	public void setDetalleVenta(ArrayList<entDetalleVenta> detalleVenta) {
		this.detalleVenta = detalleVenta;
	}
	public entDetalleVenta getDt() {
		return dt;
	}
	public void setDt(entDetalleVenta dt) {
		this.dt = dt;
	}
	
}
