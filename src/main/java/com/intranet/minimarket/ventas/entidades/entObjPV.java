package com.intranet.minimarket.ventas.entidades;

public class entObjPV {

	private int limite;
	private String fechaDesde;
	private String fechaHasta;

	public int getLimite() {
		return limite;
	}
	public void setLimite(int limite) {
		this.limite = limite;
	}
	public String getFechaInicio() {
		return fechaDesde;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaDesde = fechaInicio;
	}
	public String getFechaFin() {
		return fechaHasta;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaHasta = fechaFin;
	}
	
}
