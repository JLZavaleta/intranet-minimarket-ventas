package com.intranet.minimarket.ventas.entidades;

public class entProducto {
	
	private int codproducto;
	private entCategoria categoria;
	private String nombre;
	private String descripcion;
	private String stock;
	private String preciov;
	private int limite;
	
	public entProducto() {

	}
	
	public entProducto(String stock) {
		super();
		this.stock = stock;
	}
	
	public int getCodproducto() {
		return codproducto;
	}
	public void setCodproducto(int codproducto) {
		this.codproducto = codproducto;
	}
	public entCategoria getCategoria() {
		return categoria;
	}
	public void setCategoria(entCategoria categoria) {
		this.categoria = categoria;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public String getPreciov() {
		return preciov;
	}
	public void setPreciov(String preciov) {
		this.preciov = preciov;
	}

	public int getLimite() {
		return limite;
	}

	public void setLimite(int limite) {
		this.limite = limite;
	}	
		
}
