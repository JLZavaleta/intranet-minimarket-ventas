package com.intranet.minimarket.ventas.entidades;

public class entCliente {
	
	private int codcliente;
	private String nombre;
	private String ape_pater;
	private String ape_mater;
	private String dni;
	private String direccion;
	private String telefono;
	
	public int getCodcliente() {
		return codcliente;
	}
	public void setCodcliente(int codcliente) {
		this.codcliente = codcliente;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApe_pater() {
		return ape_pater;
	}
	public void setApe_pater(String ape_pater) {
		this.ape_pater = ape_pater;
	}
	public String getApe_mater() {
		return ape_mater;
	}
	public void setApe_mater(String ape_mater) {
		this.ape_mater = ape_mater;
	}	
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
}
