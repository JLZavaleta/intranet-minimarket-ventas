package com.intranet.minimarket.ventas.entidades;

public class entTrabajador {
	
	private int codtrabajador;
	private String nombre;
	private String ape_pater;
	private String ape_mater;
	private String direccion;
	private String telefono;
	private String usuario;
	private String passwd;
	private boolean tipo=false;
	private int cantidad;
	
	public int getCodtrabajador() {
		return codtrabajador;
	}
	public void setCodtrabajador(int codtrabajador) {
		this.codtrabajador = codtrabajador;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApe_pater() {
		return ape_pater;
	}
	public void setApe_pater(String ape_pater) {
		this.ape_pater = ape_pater;
	}
	public String getApe_mater() {
		return ape_mater;
	}
	public void setApe_mater(String ape_mater) {
		this.ape_mater = ape_mater;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public boolean isTipo() {
		return tipo;
	}
	public void setTipo(boolean tipo) {
		this.tipo = tipo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	} 
}
