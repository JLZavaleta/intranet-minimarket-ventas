package com.intranet.minimarket.ventas.entidades;

public class entDetalleVenta {
	
	private entVenta venta;
	private entProducto producto;
	private int cantidad;
	private String subtotal;
	private String valor_venta;
	
	public entVenta getVenta() {
		return venta;
	}
	public void setVenta(entVenta venta) {
		this.venta = venta;
	}
	public entProducto getProducto() {
		return producto;
	}
	public void setProducto(entProducto producto) {
		this.producto = producto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public String getValor_venta() {
		return valor_venta;
	}
	public void setValor_venta(String valor_venta) {
		this.valor_venta = valor_venta;
	}	
	public int stockActual() {
		return  Integer.parseInt(producto.getStock()) - this.cantidad;
	} 
	
}
