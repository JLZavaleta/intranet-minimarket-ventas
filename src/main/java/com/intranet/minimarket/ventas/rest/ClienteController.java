package com.intranet.minimarket.ventas.rest;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.intranet.minimarket.ventas.entidades.entCliente;
import com.intranet.minimarket.ventas.message.Response;
import com.intranet.minimarket.ventas.negocio.negCliente;

@Controller
public class ClienteController {
	
	@RequestMapping(value ="/listar", method = RequestMethod.GET,produces="application/json")
	public Response Lista() {		
		ArrayList<entCliente> lista=null;
		Response response = null;
		try {
			lista = negCliente.Instancia().ListarClientes();
			response = new Response("Done",lista);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
		
}
