package com.intranet.minimarket.ventas.negocio;

import java.util.ArrayList;

import com.intranet.minimarket.ventas.entidades.entCategoria;
import com.intranet.minimarket.ventas.persistencia.daoCategoria;

public class negCategoria {
	// Singleton
			public static negCategoria _Instancia;
			private negCategoria() {};
			public static negCategoria Instancia() {
				if (_Instancia == null) {
					_Instancia = new negCategoria();
				}
				return _Instancia;
			}
		// endSingleton	
		public ArrayList<entCategoria> ListarCategorias() throws Exception{
			try {
				ArrayList<entCategoria> lista = daoCategoria.Instancia().ListarCategorias();
				return lista;
			} catch (Exception e) {
				throw e;
			}
		}
}
