package com.intranet.minimarket.ventas.negocio;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.intranet.minimarket.ventas.entidades.entTrabajador;
import com.intranet.minimarket.ventas.persistencia.daoTrabajador;

public class negTrabajador {
	// Singleton
	public static negTrabajador _Instancia;
	private negTrabajador() {};
	public static negTrabajador Instancia() {
		if (_Instancia == null) {
			_Instancia = new negTrabajador();
		}
		return _Instancia;
	}
	// endSingleton
	public entTrabajador verificarAcceso(String _Usuario,String _Pass,boolean _x) throws Exception{		
		try {
			entTrabajador e=null;
			if(_Usuario.length()>7&&_Pass.length()>8){
				throw new ArithmeticException("El usuario y password tienen mas caracteres de lo permitido.");
			}
			if(_Usuario.length()>7){
				throw new ArithmeticException("Solo se permite 7 caracteres en el usuario.");
			}
			if(_Pass.length()>8){
				throw new ArithmeticException("Solo se permite 8 caracteres en el password.");
			}
			
			if(_Usuario.length()<=7&&_Pass.length()<=8){				
				e =  daoTrabajador.Instancia().VerificarAcceso(_Usuario, _Pass,_x);
			}
			if(e==null) {
				throw new ArithmeticException("Verifique si el tipo de cuenta es correcto.");
			}
			return e;

		} catch (Exception ex) {
			throw ex;
		}		
	}
	
	public ArrayList<entTrabajador> ListarTrabajadores() throws Exception{
		try {
			ArrayList<entTrabajador> lista = daoTrabajador.Instancia().ListarTrabajadores();
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public entTrabajador DevolverTrabajadorId(int id)throws Exception{
		try {
			entTrabajador lista = daoTrabajador.Instancia().DevolverTrabajadorId(id);
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}
		
	public boolean nuevoTrabajador(entTrabajador trabajador)throws Exception{
		try {
			validacion(trabajador);
			boolean x = daoTrabajador.Instancia().nuevoTrabajador(trabajador);
			return x;		
		} catch (Exception e) {
			throw e;
			
		}
	}
	
	public boolean EditarTrabajador(entTrabajador trabajador)throws Exception{
		try {			
			validacion(trabajador);			
			boolean x = daoTrabajador.Instancia().EditarTrabajador(trabajador);
			return x;		
		} catch (Exception e) {
			throw e;
			
		}
	}
	
	public boolean EliminarTrabajador(int id)throws Exception{
		try {
			boolean x = daoTrabajador.Instancia().EliminarTrabajador(id);
			return x;
		} catch (Exception e) {
			throw e;
			
		}
	}
	
private void validacion(entTrabajador trabajador) {

		if(trabajador.getNombre().trim().equals("")||trabajador.getNombre().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar un nombre para el Cliente.");
		}
		if(isnumeric(trabajador.getNombre().trim())){
			throw new ArithmeticException("Debe ingresar un nombre Valido.");
		}
		if(trabajador.getApe_pater().trim().equals("")||trabajador.getApe_pater().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar un apellido paterno  para el Cliente.");
		}
		if(isnumeric(trabajador.getApe_pater().trim())){
			throw new ArithmeticException("Debe ingresar un apellido paterno  Valida.");
		}
		if(trabajador.getApe_mater().trim().equals("")||trabajador.getApe_mater().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar un apellido materno  para el Cliente.");
		}
		if(isnumeric(trabajador.getApe_mater().trim())){
			throw new ArithmeticException("Debe ingresar una apellido materno Valida.");
		}
		if(trabajador.getDireccion().trim().equals("")||trabajador.getDireccion().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar una Direccion  para el Cliente.");
		}
		if(isnumeric(trabajador.getDireccion().trim())){
			throw new ArithmeticException("Debe ingresar una Direccion Valida.");
		}
				
		if(trabajador.getTelefono().trim().equals("")||trabajador.getTelefono().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar un telefono para el Cliente.");
		}
		if(!isInt(trabajador.getTelefono())||Integer.parseInt(trabajador.getTelefono())<0){
			throw new ArithmeticException("Debe ingresar un telefono Valido.");
		}
		if(trabajador.getTelefono().trim().length()>9||trabajador.getTelefono().trim().length()<9){
			throw new ArithmeticException("Debe ingresar solo 9 digitos.");
		}
		if(trabajador.getUsuario().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar un Usuario.");
		}
		if(trabajador.getPasswd().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar un password.");
		}
		if(trabajador.getUsuario().trim().length()>7){
			throw new ArithmeticException("Debe ingresar solo 7 digitos.");
		}
		if(trabajador.getPasswd().trim().length()>8){
			throw new ArithmeticException("Debe ingresar solo 8 digitos.");
		}
	}
	
	private boolean isnumeric(String dato) {

		try {
			BigDecimal bigdecimal = new BigDecimal(dato);
			BigDecimal bigdecimal1 = new BigDecimal(dato);
			@SuppressWarnings("unused")
			BigDecimal bigdecimalresultado = bigdecimal.add(bigdecimal1);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	private boolean isInt(String dato) {

		try {
			Integer.parseInt(dato);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
