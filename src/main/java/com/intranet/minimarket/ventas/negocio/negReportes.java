package com.intranet.minimarket.ventas.negocio;

import java.util.ArrayList;

import com.intranet.minimarket.ventas.entidades.entCliente;
import com.intranet.minimarket.ventas.entidades.entProducto;
import com.intranet.minimarket.ventas.entidades.entTrabajador;
import com.intranet.minimarket.ventas.entidades.entVenta;
import com.intranet.minimarket.ventas.persistencia.daoReportes;

public class negReportes {
	// Singleton
	public static negReportes _Instancia;
	public negReportes(){}
	public static negReportes Instancia() {
		if(_Instancia==null) {
			_Instancia = new negReportes();
		}
		return _Instancia;
	}
	// endSingleton
	//CLIENTES INICIO
	public ArrayList<entCliente> ClientesHabituales() throws Exception{
		try {
			ArrayList<entCliente> lista = daoReportes.Instancia().ClientesHabituales();
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}
	//CLIENTES FIN
	
	//PRODUCTOS INICIO
	public ArrayList<entProducto> productosConStockBajo() throws Exception{
		try {
			ArrayList<entProducto> lista = daoReportes.Instancia().productosConStockBajo();
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}
	public ArrayList<entProducto> productosMasVendidos() throws Exception{
		try {
			ArrayList<entProducto> lista = daoReportes.Instancia().productosMasVendidos();
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}
	public ArrayList<entProducto> productosMasVendidosLimite(int limite) throws Exception{
		try {
			ArrayList<entProducto> lista = daoReportes.Instancia().productosMasVendidosLimite(limite);
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}
	public ArrayList<entProducto> productosMasVendidosFechas(String fechaInicio, String fechaFin) throws Exception{
		try {
			ArrayList<entProducto> lista = daoReportes.Instancia().productosMasVendidosFechas(fechaInicio, fechaFin);
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}
	public ArrayList<entProducto> productosMasVendidosFechasLimite(String fechaInicio, String fechaFin, int limite) throws Exception{
		try {
			ArrayList<entProducto> lista = daoReportes.Instancia().productosMasVendidosFechasLimite(fechaInicio, fechaFin, limite);
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}
	public ArrayList<entVenta> ventasRealizadasPorTrabajador() throws Exception{
		try {
			ArrayList<entVenta> lista = daoReportes.Instancia().ventasRealizadasPorTrabajador();
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}
	public ArrayList<entTrabajador> trabajadoresConMasVentas() throws Exception{
		try {
			ArrayList<entTrabajador> lista = daoReportes.Instancia().trabajadoresConMasVentas();
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}
	//PRODUCTOS FIN
	
}