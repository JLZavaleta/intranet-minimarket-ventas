package com.intranet.minimarket.ventas.negocio;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.intranet.minimarket.ventas.entidades.entCliente;
import com.intranet.minimarket.ventas.persistencia.daoCliente;

public class negCliente {
	// Singleton
	public static negCliente _Instancia;
	private negCliente() {};
	public static negCliente Instancia() {
		if (_Instancia == null) {
			_Instancia = new negCliente();
		}
		return _Instancia;
	}
	// endSingleton
	public ArrayList<entCliente> ListarClientes() throws Exception{
		try {
			ArrayList<entCliente> lista = daoCliente.Instancia().ListarClientes();
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public entCliente DevolverClienteId(int id)throws Exception{
		try {
			entCliente lista = daoCliente.Instancia().DevolverClienteId(id);
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}

	public boolean nuevoCliente(entCliente cliente)throws Exception{
		try {
			validacion(cliente);
			boolean x = daoCliente.Instancia().nuevoCliente(cliente);
			return x;		
		} catch (Exception e) {
			throw e;
			
		}
	}
	
	public boolean EditarCliente(entCliente cliente)throws Exception{
		try {			
			validacion(cliente);			
			boolean x = daoCliente.Instancia().EditarCliente(cliente);
			return x;		
		} catch (Exception e) {
			throw e;
			
		}
	}
	
	public boolean EliminarCliente(int idcliente)throws Exception{
		try {
			boolean x = daoCliente.Instancia().EliminarCliente(idcliente);
			return x;
		} catch (Exception e) {
			throw e;
			
		}
	}
	//#codcliente, nombre, ape_pater, ape_mater, direccion, telefono
	private void validacion(entCliente cliente) {
		
		if(cliente.getDni().trim().equals("")||cliente.getDni().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar un DNI para el Cliente.");
		}
		if(!isInt(cliente.getDni())||Integer.parseInt(cliente.getDni())<0){
			throw new ArithmeticException("Debe ingresar un DNI Valido.");
		}
		if(cliente.getDni().trim().length()>8||cliente.getDni().trim().length()<8){
			throw new ArithmeticException("Debe ingresar 8 digitos.");
		}
		if(daoCliente.Instancia().DevolverClientePorDni(cliente.getDni().trim())){
			throw new ArithmeticException("Ya existe un Cliente con este Documento de Identidad.");
		}
		if(cliente.getNombre().trim().equals("")||cliente.getNombre().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar un nombre para el Cliente.");
		}
		if(isnumeric(cliente.getNombre().trim())){
			throw new ArithmeticException("Debe ingresar un nombre Valido.");
		}
		if(cliente.getApe_pater().trim().equals("")||cliente.getApe_pater().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar un apellido paterno  para el Cliente.");
		}
		if(isnumeric(cliente.getApe_pater().trim())){
			throw new ArithmeticException("Debe ingresar un apellido paterno  Valida.");
		}
		if(cliente.getApe_mater().trim().equals("")||cliente.getApe_mater().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar un apellido materno  para el Cliente.");
		}
		if(isnumeric(cliente.getApe_mater().trim())){
			throw new ArithmeticException("Debe ingresar una apellido materno Valida.");
		}
		if(cliente.getDireccion().trim().equals("")||cliente.getDireccion().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar una Direccion  para el Cliente.");
		}
		if(isnumeric(cliente.getDireccion().trim())){
			throw new ArithmeticException("Debe ingresar una Direccion Valida.");
		}
				
		if(cliente.getTelefono().trim().equals("")||cliente.getTelefono().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar un telefono para el Cliente.");
		}
		if(!isInt(cliente.getTelefono())||Integer.parseInt(cliente.getTelefono())<0){
			throw new ArithmeticException("Debe ingresar un telefono Valido.");
		}
		if(cliente.getTelefono().trim().length()>9||cliente.getTelefono().trim().length()<9){
			throw new ArithmeticException("Debe ingresar solo 9 digitos.");
		}
	}
	
	private boolean isnumeric(String dato) {

		try {
			BigDecimal bigdecimal = new BigDecimal(dato);
			BigDecimal bigdecimal1 = new BigDecimal(dato);
			@SuppressWarnings("unused")
			BigDecimal bigdecimalresultado = bigdecimal.add(bigdecimal1);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	private boolean isInt(String dato) {

		try {
			Integer.parseInt(dato);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
		
}
