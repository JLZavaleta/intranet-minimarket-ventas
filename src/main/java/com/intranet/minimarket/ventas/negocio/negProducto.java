package com.intranet.minimarket.ventas.negocio;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.intranet.minimarket.ventas.entidades.entProducto;
import com.intranet.minimarket.ventas.persistencia.daoProducto;

public class negProducto {
	// Singleton
	public static negProducto _Instancia;
	private negProducto() {};
	public static negProducto Instancia() {
		if (_Instancia == null) {
			_Instancia = new negProducto();
		}
		return _Instancia;
	}
	// endSingleton
	public ArrayList<entProducto> ListarProductos() throws Exception{
		try {
			ArrayList<entProducto> lista = daoProducto.Instancia().ListarProductos();
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}

	public entProducto DevolverProductoId(int id)throws Exception{
		try {
			entProducto lista = daoProducto.Instancia().DevolverProductoId(id);
			return lista;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public boolean nuevoProducto(entProducto producto)throws Exception{
		try {
			validacion(producto);
			boolean x = daoProducto.Instancia().nuevoProducto(producto);
			return x;		
		} catch (Exception e) {
			throw e;
			
		}
	}
	
	public boolean EditarProducto(entProducto producto)throws Exception{
		try {			
			validacion(producto);			
			boolean x = daoProducto.Instancia().EditarProducto(producto);
			return x;		
		} catch (Exception e) {
			throw e;
			
		}
	}
	
	public boolean EliminarProducto(int idproducto)throws Exception{
		try {
			boolean x = daoProducto.Instancia().EliminarProducto(idproducto);
			return x;
		} catch (Exception e) {
			throw e;
			
		}
	}
	
	private void validacion(entProducto producto) {

		if(producto.getNombre().trim().equals("")||producto.getNombre().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar un nombre para el Producto.");
		}
		if(isnumeric(producto.getNombre().trim())){
			throw new ArithmeticException("Debe ingresar un nombre Valido.");
		}
		if(producto.getDescripcion().trim().equals("")||producto.getDescripcion().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar una descripcion para el Producto.");
		}
		if(isnumeric(producto.getDescripcion().trim())){
			throw new ArithmeticException("Debe ingresar una descripcion Valida.");
		}
		if(producto.getCategoria().getIdtipo()==0){
			throw new ArithmeticException("Debe Selecionar una Categoria.");
		}
		if(producto.getPreciov().trim().equals("")){
			throw new ArithmeticException("Debe ingresar un precio para el Producto.");
		}
		if(!isnumeric(producto.getPreciov())){				
			throw new ArithmeticException("Debe ingresar un precio valido.");
		}
		if(Double.parseDouble(producto.getPreciov())<0){
			throw new ArithmeticException("Debe ingresar un precio mayor a cero.");
		}
		if(producto.getStock().trim().equals("")||producto.getStock().trim().equals("0")){
			throw new ArithmeticException("Debe ingresar un stock para el Producto.");
		}
		if(!isInt(producto.getStock())){
			throw new ArithmeticException("Debe ingresar un stock Valido.");
		}		
		if(Integer.parseInt(producto.getStock())<0){
			throw new ArithmeticException("Debe ingresar un stock mayor a cero.");
		}
	}
	
	private boolean isnumeric(String dato) {

		try {
			BigDecimal bigdecimal = new BigDecimal(dato);
			BigDecimal bigdecimal1 = new BigDecimal(dato);
			@SuppressWarnings("unused")
			BigDecimal bigdecimalresultado = bigdecimal.add(bigdecimal1);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	private boolean isInt(String dato) {

		try {
			Integer.parseInt(dato);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
		
}
