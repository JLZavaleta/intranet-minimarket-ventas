package com.intranet.minimarket.ventas.negocio;

import java.util.ArrayList;

import com.intranet.minimarket.ventas.entidades.entVenta;
import com.intranet.minimarket.ventas.persistencia.daoVenta;

public class negVenta {
	
	// Singleton
	public static negVenta _Instancia;
	private negVenta() {};
	public static negVenta Instancia() {
		if (_Instancia == null) {
			_Instancia = new negVenta();
		}
		return _Instancia;
	}
	// endSingleton
	
	public boolean nuevaVenta(entVenta venta)throws Exception{
		boolean x =false;
		try {
			//validacion(venta);
			x = daoVenta.Instancia().nuevaVenta(venta);
			if(x) {
				return true;
			}
			
		} catch (Exception e) {			 
			throw e;
		}
		return x;
	}
	
	public ArrayList<entVenta> ventasRealizadas()throws Exception{
		ArrayList<entVenta> lista = null;
		try {
			lista = daoVenta.Instancia().ventasRealizadas();
		} catch (Exception e) {
			throw e;
		}
		return lista;
	}
	
	public entVenta ventaPdf(int id)throws Exception{
		entVenta venta = null;
		try {
			venta = daoVenta.Instancia().ventaPdf(id);
		} catch (Exception e) {
			throw e;
		}
		return venta;
	}
	
	@SuppressWarnings({ "unlikely-arg-type", "unused" })
	private void validacion(entVenta venta) {
		if(venta.getCliente().getNombre().trim().equals("")||venta.getCliente().getNombre().trim().isEmpty()){
			throw new ArithmeticException("Debe ingresar un Cliente.");
		}
		if(venta.getTipodoc().trim().equals("0")||venta.getCliente().getNombre().trim().isEmpty()){
			throw new ArithmeticException("Debe un Tipo de Documento.");
		}
		if(venta.getF_emision().equals("")){
			throw new ArithmeticException("Debe una Fecha de Emision.");
		}
		if(venta.getDetalleVenta()==null || venta.getDetalleVenta().size()>-1){
			throw new ArithmeticException("No ingreso ningun Producto.");
		}
	}
	
}
